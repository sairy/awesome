local beau = require('beautiful')
local gtable = require('gears.table')

---@class text_element
---@field fg?   string
---@field text? string
---@field font? Font

local M = {}

---@param t text_element?
---@return text_element
M.new_elem = function (t)
    return gtable.crush({
        fg = beau.fg,
        text = ' ..',
        font = beau.fnt,
    }, t or {})
end

---@param elem text_element
---@return string
M.fontfg = function (elem)
    elem = M.new_elem(elem)

    return string.format(
        '<span font="%s" foreground="%s">%s</span>',
        elem.font:as_str(),
        elem.fg,
        elem.text
    )
end

return M
