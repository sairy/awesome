local awe = require('awful')
local gmath = require('gears.math')

local kb = require('conf.default').keyboard

local At = kb.initial

local M = {}

M.reset = function ()
    return M.switchto(kb.layouts[kb.initial])
end

M.switchto = function (layout)
    awe.spawn.with_shell(kb.cmd .. ' ' .. layout)
end

M.next = function ()
    At = (At % #kb.layouts) + 1
    return M.switchto(kb.layouts[At])
end

M.prev = function ()
    At = gmath.cycle(#kb.layouts, At - 1) --[[@as integer]]
    return M.switchto(kb.layouts[At])
end

return M
