local default_base = require('conf.default').unit_base

-- Strings used as keys instead of numbers
-- to avoid creating a sparse array
local prefixes = {
    ['1000'] = { 'K', 'M', "G", 'T', 'P', 'E', 'Z', 'Y' },
    ['1024'] = { 'Ki', 'Mi', "Gi", 'Ti', 'Pi', 'Ei', 'Zi', 'Yi' },
}

local M = {}

---Format a number according to the base
--e.g: fmt(1024, 1024) -> '1 Ki'
---@param x number number to format
---@param base integer? base to use, defaults to conf.defaults.unit_base
---@return string
M.fmt = function (x, base)

    base = tonumber(base) or default_base or 1024

    local pfx = assert(prefixes[tostring(base)], 'unknown base: ' .. base)

    if x < base then
        return x .. ' '
    end

    local scaled = x / base

    local i = 1
    while i <= #pfx and scaled >= base do
        scaled = scaled / base
        i = i + 1
    end

    return string.format('%.1f %s', scaled, pfx[i])
end

return M
