local beau = require('beautiful')
local gtable = require('gears.table')
local wibox = require('wibox.init')

local markup = require('util.markup')

local dpi = beau.xresources.apply_dpi

local M = {}

---@class widget.margins: Screen.margins

---@param widget table
---@param icon string|text_element?
M.update_icon = function (widget, icon)
    if not icon then
        return
    elseif type(icon) == 'string' then
        icon = { text = icon --[[@as string]] }
    end

    -- indirectly updates __prev_elem
    icon = gtable.crush(widget.margin.horiz.icon.__prev_elem, icon or {})

    widget.margin.horiz.icon.markup = markup.fontfg(icon)
end

---@param widget table
---@param text string|text_element?
M.update_text = function (widget, text)
    if not text then
        return
    elseif type(text) == 'string' then
        text = { text = text --[[@as string]] }
    end

    -- indirectly updates __prev_elem
    text = gtable.crush(widget.margin.horiz.text.__prev_elem, text or {})

    widget.margin.horiz.text.markup = markup.fontfg(text)
end

---@param widget table
---@param icon string|text_element?
---@param text string|text_element?
M.update_markup = function (widget, icon, text)
    M.update_icon(widget, icon)
    M.update_text(widget, text)
end

---@param m  integer|widget.margins
---@param s? Screen
---@return widget.margins
M.margins = function (m, s)
    if type(m) == 'number' then
        local t = {}
        for _, pos in ipairs { 'top', 'bottom', 'left', 'right' } do
            t[pos] = dpi(m, s)
        end
        return t
    else
        ---@cast m -integer
        for pos, value in pairs(m) do
            m[pos] = dpi(value, s)
        end
        return m
    end
end

---@class base_widget.opts
---@field clickable boolean add hover effect

---@param icon text_element
---@param val text_element
---@param opts base_widget.opts?
---@return table #widget
M.base_widget = function (icon, val, opts)
    icon = markup.new_elem(icon)
    val = markup.new_elem(val)
    opts = opts or {}

    local widget = wibox.widget {
        widget = wibox.container.background,

        {
            widget = wibox.container.margin,
            id = 'margin',
            margins = M.margins {
                left = 10,
                right = 10,
            },
            {
                layout = wibox.layout.fixed.horizontal,
                id = 'horiz',
                {
                    widget = wibox.widget.textbox,
                    id = 'icon',
                    markup = markup.fontfg(icon),
                    halign = 'center',
                    valign = 'center',
                    -- font = icon.font:as_str(),
                    -- text = icon.text,
                    __prev_elem = icon,
                },
                {
                    widget = wibox.widget.textbox,
                    id = 'text',
                    markup = markup.fontfg(val),
                    halign = 'center',
                    valign = 'center',
                    -- font = val.font:as_str(),
                    -- text = val.text,
                    __prev_elem = val,
                },
            },
        },
    }

    if opts.clickable then
        M.hover_effect.cursor(widget)
    end

    return widget
end

M.hover_effect = {
    ---@param widget Object
    cursor = function (widget)
        local oldcur, oldbox
        widget:connect_signal('mouse::enter', function ()
            local box = mouse.current_wibox
            if box then
                oldcur, oldbox = box.cursor, box
                box.cursor = beau.cursors.click
            end
        end)

        widget:connect_signal('mouse::leave', function ()
            if oldbox then
                oldbox.cursor = oldcur
                oldbox = nil
            end
        end)
    end,

    ---@param widget table #widget Object
    ---@param colors { bg: string, fg: string }
    color = function (widget, colors)
        local oldbg, oldfg
        local layer = widget.background or widget

        widget:connect_signal('mouse::enter', function ()
            if colors.bg then
                oldbg, layer.bg = layer.bg, colors.bg
            end

            if colors.fg then
                oldfg, layer.fg = layer.fg, colors.fg
            end
        end)

        widget:connect_signal('mouse::leave', function ()
            layer.bg = oldbg
            layer.fg = oldfg
        end)
    end,
}

return M
