---Imutable font abstraction
---Allows for using the same font with
---different sizes in multiple places without
---using hacks to alter the font size
---@class Font
---@field private _fnt string
---@field private _size number?
local Font = {}

local cache = setmetatable({}, { __mode = 'v' })

---Define a new font
---@param font string
---@param size number
---@return Font
---@overload fun(self: Font, font: string):Font
function Font:new(font, size)
    assert(size == nil or size > 0, 'Font size must be positive or left unspecified')

    local hash = font .. (size and ' ' .. size or '')
    if cache[hash] then
        return cache[hash]
    end

    local fnt = setmetatable({ _fnt = font, _size = size }, {
        __index = self,
        __tostring = Font.as_str,
        __call = Font.new,
    })

    cache[fnt:as_str()] = fnt
    return fnt
end

---@return number?
function Font:size()
    return self._size
end

---String representation of the font
---@return string
function Font:as_str()
    return self._fnt .. (self._size and ' ' .. self._size or '')
end

---Create new font of different size
---@param size number positive number
---@return Font
function Font:with_size(size)
    return Font:new(self._fnt, size)
end

return Font
