local awe = require('awful')
local beau = require('beautiful')
local naughty = require('naughty')

--TODO: deprecated
require('awful.autofocus')

client.connect_signal('mouse::enter', function (c)
    c:activate { context = 'mouse_enter', raise = false }
end)


tag.connect_signal('request::default_layouts', function ()
    local suit = require('awful.layout.suit')
    awe.layout.append_default_layouts {
        suit.tile, -- default
        suit.floating,
        suit.max,
    }
end)


client.connect_signal('request::manage', function (c, ctx)
    -- center floating clients
    if c.floating and ctx == 'new' then
        c.placement = awe.placement.centered
    end

    -- if no tags are selected, jump to the client's first tag
    local s = awe.screen.focused()
    if s.selected_tag == nil then
        local tag = c.first_tag or s.tags[1]
        if #c:tags() == #s.tags then
            c:tags { tag }
        end
        tag:view_only()
    end

    -- autofocus spawned client
    c:activate { context = 'mouse_enter', raise = false }
end)

screen.connect_signal('request::wallpaper', function (s)
    require('ui.wallpaper').xwallpaper(s)
end)

tag.connect_signal('property::layout', function (t)
    local layout = t.layout
    t.gap = layout == awe.layout.suit.max and 0 or beau.useless_gap or 0
end)

naughty.connect_signal('request::display', function (n)
    naughty.layout.box { notification = n }
end)

naughty.connect_signal('request::display_error', function (message, startup)
    naughty.notification {
        urgency = 'critical',
        title = 'Oops, an error happened' .. (startup and ' during startup' or '!'),
        message = message,
    }
end)
