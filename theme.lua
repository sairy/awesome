local assets = require('beautiful.theme_assets')
local gfs = require('gears.filesystem')

local ast = require('theme.assets')
local Font = require('util.font')

local dpi = require('beautiful.xresources').apply_dpi
local recolor = require('gears.color').recolor_image

---@class BaseTheme: Beautiful.theme
local theme = {}

theme.cursors = {
    pointer = 'pointer',
    click = 'hand1',
    load = 'wait',
    beam = 'xterm',
}

theme.ft_size = dpi(12)
theme.fnt = Font:new('Monaco', theme.ft_size)
theme.font_kanji = Font:new('Source Han Code JP N', theme.ft_size)
theme.font_mono = Font:new('FiraCode Nerd Font', theme.ft_size)
theme.font_nerd = theme.font_mono -- alias

--- needed by beautiful
theme.font = theme.fnt:as_str()

theme.bg = '#1e1e2e'
theme.accent = '#f5c2e7'
theme.fg = theme.accent
theme.gray2 = '#6c7086'

theme.opacity = 0.80
theme.transparency = string.format('%x', math.round(theme.opacity * 255))

theme.bg_normal = theme.bg
theme.bg_focus = theme.accent
theme.bg_systray = theme.bg
theme.fg_normal = theme.accent
theme.fg_focus = theme.bg

theme.border_normal = '#595959'
theme.border_focus = theme.accent

theme.taglist_fg_focus = theme.bg
theme.taglist_bg_focus = theme.accent

theme.border_width = dpi(2)
theme.master_width_factor = 0.55

theme.snapper_gap = theme.useless_gap
theme.snap_border_width = theme.useless_gap

theme.useless_gap = dpi(10)
theme.gap_single_client = true
theme.gaps = {
    backup = theme.useless_gap,
    change_amt = dpi(1),
}

theme.swallowing = {
    ignore_floating = true,
    terminals = {
        class = {
            'st-256color',
            'kitty',
        },
    },
    excludes = {
        class = {
            'firefox',
            'librewolf',
            'nsxiv',
        },
        name = { 'Event Tester' },
        role = { 'pop-up' },
    },
}

do
    local cfg = gfs.get_configuration_dir()
    local wallpath

    if gfs.file_readable(cfg .. '.wall') then
        wallpath = cfg .. '.wall'
    else
        wallpath = cfg .. 'assets/wallpapers/xiao-57friend.jpeg'
    end

    theme.wallpaper = wallpath
end

local icondir = os.getenv('HOME') .. '/.local/share/icons/BeautyLine'
theme.icon_theme = gfs.is_dir(icondir) and icondir or nil

do
    local tpath = gfs.get_themes_dir()

    theme.awesome_icon = assets.awesome_icon(dpi(15), theme.accent, theme.bg)

    theme.bg_urgent = '#f38ba8'
    theme.fg_urgent = '#11111b'

    theme.bg_minimize = '#444444'
    theme.fg_minimize = '#ffffff'


    local sqr_size = dpi(6)
    theme.taglist_squares_sel = ast.taglist_squares_sel(sqr_size, theme.bg, 1, 1)
    theme.taglist_squares_unsel = ast.taglist_squares_unsel(
        sqr_size, theme.accent, 1, 1
    )

    theme.menu_submenu_icon = tpath .. 'default/submenu.png'
    theme.menu_height = dpi(15)
    theme.menu_width = dpi(100)


    theme.titlebar_close_button_normal = tpath .. 'default/titlebar/close_normal.png'
    theme.titlebar_close_button_focus = tpath .. 'default/titlebar/close_focus.png'

    theme.titlebar_minimize_button_normal = tpath ..
        'default/titlebar/minimize_normal.png'
    theme.titlebar_minimize_button_focus = tpath ..
        'default/titlebar/minimize_focus.png'

    theme.titlebar_ontop_button_normal_inactive = tpath ..
        'default/titlebar/ontop_normal_inactive.png'
    theme.titlebar_ontop_button_focus_inactive = tpath ..
        'default/titlebar/ontop_focus_inactive.png'
    theme.titlebar_ontop_button_normal_active = tpath ..
        'default/titlebar/ontop_normal_active.png'
    theme.titlebar_ontop_button_focus_active = tpath ..
        'default/titlebar/ontop_focus_active.png'

    theme.titlebar_sticky_button_normal_inactive = tpath ..
        'default/titlebar/sticky_normal_inactive.png'
    theme.titlebar_sticky_button_focus_inactive = tpath ..
        'default/titlebar/sticky_focus_inactive.png'
    theme.titlebar_sticky_button_normal_active = tpath ..
        'default/titlebar/sticky_normal_active.png'
    theme.titlebar_sticky_button_focus_active = tpath ..
        'default/titlebar/sticky_focus_active.png'

    theme.titlebar_floating_button_normal_inactive = tpath ..
        'default/titlebar/floating_normal_inactive.png'
    theme.titlebar_floating_button_focus_inactive = tpath ..
        'default/titlebar/floating_focus_inactive.png'
    theme.titlebar_floating_button_normal_active = tpath ..
        'default/titlebar/floating_normal_active.png'
    theme.titlebar_floating_button_focus_active = tpath ..
        'default/titlebar/floating_focus_active.png'

    theme.titlebar_maximized_button_normal_inactive = tpath ..
        'default/titlebar/maximized_normal_inactive.png'
    theme.titlebar_maximized_button_focus_inactive = tpath ..
        'default/titlebar/maximized_focus_inactive.png'
    theme.titlebar_maximized_button_normal_active = tpath ..
        'default/titlebar/maximized_normal_active.png'
    theme.titlebar_maximized_button_focus_active = tpath ..
        'default/titlebar/maximized_focus_active.png'

    -- You can use your own layout icons like this:
    theme.layout_fairh = recolor(tpath .. 'default/layouts/fairhw.png', theme.accent)
    theme.layout_fairv = recolor(tpath .. 'default/layouts/fairvw.png', theme.accent)
    theme.layout_floating = recolor(tpath .. 'default/layouts/floatingw.png', theme.accent)
    theme.layout_magnifier = recolor(tpath .. 'default/layouts/magnifierw.png',
        theme.accent)
    theme.layout_max = recolor(tpath .. 'default/layouts/maxw.png', theme.accent)
    theme.layout_fullscreen = recolor(tpath .. 'default/layouts/fullscreenw.png',
                                      theme.accent)
    theme.layout_tilebottom = recolor(tpath .. 'default/layouts/tilebottomw.png',
                                      theme.accent)
    theme.layout_tileleft = recolor(tpath .. 'default/layouts/tileleftw.png', theme.accent)
    theme.layout_tile = recolor(tpath .. 'default/layouts/tilew.png', theme.accent)
    theme.layout_tiletop = recolor(tpath .. 'default/layouts/tiletopw.png', theme.accent)
    theme.layout_spiral = recolor(tpath .. 'default/layouts/spiralw.png', theme.accent)
    theme.layout_dwindle = recolor(tpath .. 'default/layouts/dwindlew.png', theme.accent)
    theme.layout_cornernw = recolor(tpath .. 'default/layouts/cornernww.png', theme.accent)
    theme.layout_cornerne = recolor(tpath .. 'default/layouts/cornernew.png', theme.accent)
    theme.layout_cornersw = recolor(tpath .. 'default/layouts/cornersww.png', theme.accent)
    theme.layout_cornerse = recolor(tpath .. 'default/layouts/cornersew.png', theme.accent)
end

return theme
