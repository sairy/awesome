pcall(require, 'luarocks.loader')

local awe = require('awful')
local beau = require('beautiful')
local gfs = require('gears.filesystem')

-- load our extensions to the base libraries
require('extensions')

local theme = gfs.get_configuration_dir() .. 'theme.lua'
beau.init(theme)

require('signals')

awe.spawn.with_shell(gfs.get_configuration_dir() .. 'bin/autostart.sh')

-- enable window snapping
awe.mouse.snap.edge_enabled = true

local conf = require('conf.default')

do
    -- Create the tags
    local tags = conf.tags
    local layout = tags.default_layout

    if type(layout) == 'string' then
        layout = awe.layout.suit[layout]
    end

    awe.screen.connect_for_each_screen(function (s)
        awe.tag(tags.names, s, layout)
    end)
end

require('conf.keys')
require('conf.rules')

require('ui')

-- make audio widget work on startup
awesome.emit_signal('audio::changed', 'tog')

do
    if conf.tags.start_on == 'none' then
        awe.tag.viewnone(awe.screen.focused())
    else
        local idx = math.clamp(conf.tags.start_on --[[@as integer]], 1, #conf.tags.names)
        awe.screen.focused().tags[idx]:view_only()
    end
end

if conf.window_swallowing then
    awesome.connect_signal('startup', function ()
        require('ui.swallowing').enable()
    end)
end

-- default keyboard layout
require('util.keyboard').reset()

if _G._VERSION == 'Lua 5.4' then
    collectgarbage('incremental', 110, 1000, 0)
    collectgarbage('incremental', 110, 1000, 0)
end
