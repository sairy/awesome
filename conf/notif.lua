local awe = require('awful')
local naughty = require('naughty')

local theme = require('beautiful').get() --[[@as BaseTheme]]

naughty.config = {
    -- padding = 2.0 * conf.right,
    spacing = theme.useless_gap,
    icon_dirs = {
        theme.icon_theme,
        '/usr/share/pixmaps/',
    },

    defaults = {
        ontop = true,
        screen = awe.screen.focused(),
        timeout = 3,
        position = 'top_right',
    },

    presets = {
        -- priority
        low = {
            -- bg =
            -- fg =
        },

        normal = {
            -- bg =
            -- fg =
        },

        critical = {
            -- bg =
            -- fg =
            timeout = 0,
        },
    },
}
