local awe = require('awful')
local beau = require('beautiful')
local rule = require('ruled')

local placement = awe.placement
local tags = require('conf.default').tags.names

rule.client.connect_signal('request::rules', function ()
    rule.client.append_rule {
        id = 'global',
        rule = {},
        border_width = beau.border_width,
        border_color = beau.border_color,
        focus = awe.client.focus.filter,
        raise = true,

        maximized_vertical = false,
        maximized_horizontal = false,

        screen = awe.screen.preferred,
        placement = placement.no_overlap + placement.no_offscreen,
    }

    rule.client.append_rules {
        {
            rule_any = {
                class = { 'firefox', 'librewolf' },
            },
            properties = { tag = tags[2] },
        },
        {
            rule = { class = 'krita' },
            properties = { tag = tags[7] },
        },
    }

    rule.client.append_rule {
        id = 'floating',
        rule_any = {
            instance = {},
            class = {
                -- 'mpv',
                'generic',
                'KeePassXC',
                'MEGAsync',
                'Nsxiv',
                'kruler',
                'kcalc',
            },
            name = { 'Event Tester', 'Picture-in-Picture' },
            role = { 'pop-up' },
        },
        properties = {
            floating = true,
            titlebars_enabled = true,
            placement = placement.centered,
        },
    }

    rule.client.append_rule {
        rule_any = {
            role = { 'pop-up' },
            type = { 'dialog' },
        },
        properties = {
            fullscreen = false,
            maximized = false,
        },
    }
end)

rule.notification.connect_signal('request::rules', function ()
    rule.notification.append_rule {
        rule = {},
        properties = {
            screen = awe.screen.preferred,
            implicit_timeout = 5,
        },
    }
end)

-- floating clients should remain on top after exiting fullscreen
client.connect_signal('request::geometry', function (c, ctx)
    if ctx == 'fullscreen' and not c.fullscreen then
        c.ontop = c.floating
    end
end)

-- make floating clients stay on top
client.connect_signal('property::floating', function (c)
    if not c.fullscreen then
        c.ontop = c.floating
    end
end)
