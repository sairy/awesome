local awe = require('awful')

local conf = require('conf.default')
local gaps = require('ui.dynamic_gaps')
local kbd = require('util.keyboard')

local mod = conf.mod

local env = {
    scr = require('env.screenshot'),
    light = require('env.backlight'),
    audio = require('env.audio'),
}

local num_tags = #conf.tags.names

-- Globals {{{
awe.keyboard.append_global_keybindings {
    awe.key({ mod, 'Shift' }, 'q', awesome.quit),
    awe.key({ mod, 'Control' }, 'q', awesome.restart),

    awe.key({ mod, conf.alt }, 'q', function ()
        awesome.emit_signal('prompt::powermenu::show')
    end),

    awe.key({ mod }, 'Tab', function ()
        awe.tag.history.restore(awe.screen.focused(), 'previous')
    end),

    awe.key({ mod, 'Shift' }, 'space', function ()
        awe.layout.inc(-1)
    end),

    awe.key({ mod }, 'Print', function ()
        awe.spawn.with_shell(env.scr:choose_area())
    end),
    awe.key({ mod, 'Shift' }, 'Print', function ()
        awe.spawn.with_shell(env.scr:full())
    end),

    -- #50 => shift
    awe.key({ conf.alt }, '#50', kbd.next),

    awe.key({ mod }, 'b', function ()
        awesome.emit_signal('wibox::toggle')
    end),

    awe.key({ mod, 'Shift' }, 'n', function ()
        local c = awe.client.restore()
        if c then
            c:activate { context = 'client_restore', raise = true }
        end
    end),

    -- Windows
    awe.key({ mod }, 'l', function ()
        awe.tag.incmwfact(0.05)
    end),

    awe.key({ mod }, 'h', function ()
        awe.tag.incmwfact(-0.05)
    end),

    awe.key({ mod }, 'j', function ()
        awe.client.focus.byidx(1)
    end),

    awe.key({ mod }, 'k', function ()
        awe.client.focus.byidx(-1)
    end),

    awe.key({ mod, 'Shift' }, 'j', function ()
        awe.client.swap.byidx(1)
    end),

    awe.key({ mod, 'Shift' }, 'k', function ()
        awe.client.swap.byidx(-1)
    end),


    -- Programs
    awe.key({ mod, 'Shift' }, 'Return', function ()
        awe.spawn.with_shell(conf.term)
    end),

    awe.key({ mod }, 'o', function ()
        awe.spawn.with_shell(conf.browser)
    end),

    awe.key({ mod }, 'p', function ()
        awe.spawn.with_shell(conf.menu)
    end),

    -- gaps
    awe.key({ mod }, '=', gaps.reset),
    awe.key({ mod }, '-', gaps.decf),
    awe.key({ mod, 'Shift' }, '=', gaps.incf),
    awe.key({ mod, 'Shift' }, '-', gaps.toggle),

    -- layouts
    awe.key({ mod }, 't', function ()
        awe.layout.set(awe.layout.suit.tile)
    end),

    awe.key({ mod }, 'm', function ()
        local focused = client.focus
        awe.layout.set(awe.layout.suit.max)

        -- awesome does this by default, **EXCEPT**
        -- when you toggle monocle for the first time
        if focused then
            focused:activate { context = 'monocle', raise = true }
        end
    end),

    awe.key({ mod }, 'f', function ()
        awe.layout.set(awe.layout.suit.floating)
    end),
}
-- }}}

-- Client bindings {{{

---@type Client?
local Old_master

client.connect_signal('request::default_keybindings', function ()
    awe.keyboard.append_client_keybindings {
        awe.key({ mod }, 'q', function (c)
            if c.swallowed then
                c:swap(c.swallowed)
            end
            c:kill()
        end),

        awe.key({ mod }, 'space', awe.client.floating.toggle),

        ---@param c Client
        awe.key({ mod }, 'Return', function (c)
            local master = awe.client.getmaster() --[[@as Client]]

            if c ~= master then
                c:swap(master)
            elseif Old_master and c ~= Old_master then
                c:swap(Old_master)
            else
                c:swap(awe.client.next(1))
            end

            Old_master = master

            local m = awe.client.getmaster() --[[@as Client]]
            m:activate { context = 'zoomswap', raise = true }
        end),

        awe.key({ mod }, 'c', awe.placement.centered),

        awe.key({ mod, 'Shift' }, 'c', function (c)
            c.ontop = not c.ontop
        end),

        awe.key({ mod }, 'n', function (c)
            c.minimized = true
        end),
    }
end)

client.connect_signal('request::unmanage', function (c, ctx)
    if c == Old_master and ctx == 'destroyed' then
        Old_master = nil
    end
end)
-- }}}

-- Mouse bindings {{{
client.connect_signal('request::default_mousebindings', function ()
    local bindings = {
        awe.button({}, 1, function (c)
            c:emit_signal('request::activate', 'mouse_click', { raise = true })
        end),

        awe.button({ mod }, 1, function (c)
            c:emit_signal('request::activate', 'mouse_click', { raise = true })
            c.floating = true
            awe.mouse.client.move(c)
        end),

        awe.button({ mod }, 3, function (c)
            c:emit_signal('request::activate', 'mouse_click', { raise = true })
            awe.mouse.client.resize(c, 'bottom_right')
        end),
    }

    local mv = conf.resize_scroll
    ---@format disable-next
    local t = {
        { 0,  mv }, -- down
        { 0, -mv }, -- up
        { mv,  0 }, -- right
        { -mv, 0 }, -- left
    }

    ---mouse button offset
    local offset = 3
    for i, v in ipairs(t) do
        ---@param c Client
        bindings[#bindings+1] = awe.button({ mod }, i + offset, function (c)
            c:emit_signal('request::activate', 'mouse_click', { raise = true })
            c.floating = true

            c:relative_move(0, 0, v[1], v[2])
            mouse.coords({ x = c.x + c.width, y = c.y + c.height }, true)
        end)
    end

    awe.mouse.append_client_mousebindings(bindings)
end)
-- }}}

-- Tagging {{{

---@param keygroup {[1]: string, [2]: integer}[]
local function bindtags(keygroup)
    for i = 1, math.min(num_tags, #keygroup) do
        local key = keygroup[i]
        local sym, idx = key[1], key[2]

        awe.keyboard.append_global_keybindings {
            awe.key({ mod }, sym, function ()
                local tag = awe.screen.focused().tags[idx]
                if tag then
                    tag:view_only()
                end
            end),

            awe.key({ mod, 'Control' }, sym, function ()
                local tag = awe.screen.focused().tags[idx]
                if tag then
                    awe.tag.viewtoggle(tag)
                end
            end),
        }

        awe.keyboard.append_client_keybindings {
            ---@param c Client
            awe.key({ mod, 'Shift' }, sym, function (c)
                local s = c.screen
                if s.tags[idx] then
                    c:move_to_tag(s.tags[idx])
                    awe.layout.arrange(s)
                end
            end),

            ---@param c Client
            awe.key({ mod, 'Control', 'Shift' }, sym, function (c)
                local s = c.screen
                if s.tags[idx] then
                    c:toggle_tag(s.tags[idx])
                end
            end),
        }
    end
end

bindtags(awe.key.keygroups.numrow)
bindtags(awe.key.keygroups.numpad)

local zero_nr, zero_np = (function ()
    local grp = awe.key.keygroups
    return grp.numrow[#grp.numrow][1], grp.numpad[#grp.numpad][1]
end)()

---@type table<Screen,Tag[]>
local Selected_tags = setmetatable({}, { __mode = 'k' })
---@type table<Client, Tag[]>
local Attached_tags = setmetatable({}, { __mode = 'k' })

for _, sym in ipairs { zero_nr, zero_np } do
    awe.keyboard.append_global_keybinding(
        awe.key({ mod }, sym, function ()
            local s = awe.screen.focused()
            local sel = s.selected_tags
            local tags = s.tags

            if #sel < #tags then
                Selected_tags[s] = sel
                awe.tag.viewmore(tags, s)
            elseif #Selected_tags[s] ~= 0 then
                awe.tag.viewmore(Selected_tags[s], s)
                Selected_tags[s] = {}
            else
                awe.tag.viewnone(s)
                Selected_tags[s] = {}
            end
        end)
    )

    awe.keyboard.append_client_keybinding(
        ---@param c Client
        awe.key({ mod, 'Shift' }, sym, function (c)
            local s = c.screen
            local sel = c:tags()
            local tags = s.tags

            if #sel < #tags then
                Attached_tags[c] = sel
                c:tags(tags)
            elseif #Attached_tags[c] ~= 0 then
                c:tags(Attached_tags[c])
                Attached_tags[c] = {}
                awe.layout.arrange(s)
            else
                c:tags { s.selected_tag }
                Attached_tags[c] = {}
            end
        end)
    )
end
-- }}}

-- Fn keys {{{
awe.keyboard.append_global_keybindings {
    -- Audio
    awe.key({}, 'XF86AudioRaiseVolume', function ()
        awe.spawn.with_line_callback(env.audio:inc(), {
            exit = function ()
                awesome.emit_signal('audio::changed', 'inc')
            end,
        })
    end),

    awe.key({}, 'XF86AudioLowerVolume', function ()
        awe.spawn.with_line_callback(env.audio:dec(), {
            exit = function ()
                awesome.emit_signal('audio::changed', 'dec')
            end,
        })
    end),

    awe.key({}, 'XF86AudioMute', function ()
        awe.spawn.with_line_callback(env.audio:tog(), {
            exit = function ()
                awesome.emit_signal('audio::changed', 'tog')
            end,
        })
    end),

    awe.key({ 'Shift' }, 'XF86AudioRaiseVolume', function ()
        awe.spawn.with_shell(env.audio:inc(nil, true))
    end),

    awe.key({ 'Shift' }, 'XF86AudioLowerVolume', function ()
        awe.spawn.with_shell(env.audio:dec(nil, true))
    end),

    -- Mic
    awe.key({}, 'XF86AudioMicMute', function ()
        awe.spawn.with_shell(env.audio:tog(true))
    end),

    -- Calculator
    awe.key({}, 'XF86Calculator', function ()
        awe.spawn.with_shell(conf.calc)
    end),

    -- Brightness
    awe.key({}, 'XF86MonBrightnessUp', function ()
        awe.spawn.with_shell(env.light:inc())
    end),
    awe.key({}, 'XF86MonBrightnessDown', function ()
        awe.spawn.with_shell(env.light:dec())
    end),

    -- Media keys
    awe.key({}, 'XF86AudioPlay', function ()
        awe.spawn.with_shell('playerctl play-pause')
    end),
    awe.key({}, 'XF86AudioStop', function ()
        awe.spawn.with_shell('playerctl stop')
    end),
    awe.key({}, 'XF86AudioPrev', function ()
        awe.spawn.with_shell('playerctl previous')
    end),
    awe.key({}, 'XF86AudioNext', function ()
        awe.spawn.with_shell('playerctl next')
    end),
}
-- }}}
