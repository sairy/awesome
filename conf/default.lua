local Font = require('util.font')

local M = {}

M.mod = 'Mod4'
M.alt = 'Mod1'
M.resize_scroll = 30

M.term = 'kitty'
M.browser = 'firefox'
M.menu = 'dmenu_run -x 20 -y 10 -z 1878 -bh 4 -p "Launch: "'
M.calc = 'kcalc'

M.unit_base = 1024

M.window_swallowing = true

---@alias awful.layout 'corner'|'fair'|'floating'|'magnifier'|'max'|'spiral'|'tile'

M.tags = {
    names = { '一', '二', '三', '四', '五', '六', '七', '八', '九' },
    ---@type integer|'none'
    start_on = 'none',
    ---@type table|awful.layout
    default_layout = 'tile',
}

M.osd = {
    powermenu = {
        username = nil,
        avatar = nil,
        screenlocker = 'slock',
        font = Font:new('SF Pro Rounded', 16),
        default_action = {
            timeout = 10,
            comm = '/sbin/poweroff',
            phrase = 'Shutting down',
        },
    },
}

M.screenshot = {
    backend = 'flameshot',
}

M.brightness = {
    backend = 'light',
    ---@type Backlight.props
    props = {
        step = 5,
    },
}

M.keyboard = {
    cmd = 'setxkbmap',

    layouts = { 'us', 'pt', 'gr' },
    initial = 1,
}

M.bar_powermenu = {
    icon = ' ',
}

M.audio = {
    icons = {
        muted = '󰖁 ',
        quiet = '󰖀 ',
        loud = '󰕾 ',
    },

    backend = 'pulseaudio',
    ---@type Audio.props
    props = {
        step = 2,
        -- sink = 'alsa_output.pci-0000_03_00.6.analog-stereo',
        -- src  = 'alsa_input.pci-0000_03_00.6.analog-stereo',
        sink = '@DEFAULT_SINK@',
        source = '@DEFAULT_SOURCE@',
    },
}

M.bat = {
    pfx = '/sys/class/power_supply/',
    battery = {
        name = 'BAT0',
        timeout = 5,
        ---@type 'perc'|'rem'
        show = 'rem',
    },
    charger = {
        name = 'ADP0',
        timeout = 1,
        ---@type 'perc'|'rem'
        show = 'perc',
    },
    icons = {
        empty = '  ',
        half = '  ',
        almost = '  ',
        full = '󰣐 ',
        animation = { '  ', '  ', '  ', '  ' },
    },
}

do
    local pfx = M.bat.pfx
    ---@param self { name:string, timeout:integer }
    ---@return string
    local bat_path = function (self)
        return pfx .. self.name .. '/'
    end
    M.bat.battery.path = bat_path
    M.bat.charger.path = bat_path
end

M.net = {
    pfx = '/sys/class/net/',
    rx = '/statistics/rx_bytes',
    tx = '/statistics/tx_bytes',
    wifi = 'wlp1s0',
    ether = 'enp3s0f3u2',
    loop = 'lo',

    ---@type integer
    timeout = 1,
    manager = 'networkmanager_dmenu -x 20 -y 10 -z 1878 -bh 4',

    ---@type 'rx'|'tx'
    which = 'rx',
    icon = '󰇚 ', -- '󰕒 '
}

return M
