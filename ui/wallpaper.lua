local awe = require('awful')
local beau = require('beautiful')
local gea = require('gears')
local wibox = require('wibox.init')

local M = {}

---@param s?    Screen
---@param wal?  string|fun(Screen):string
local basic = function (s, wal)
    s = s or awe.screen.focused()
    local wp = wal or beau.wallpaper

    if type(wp) == 'function' then
        wp = wp(s)
    end

    return gea.wallpaper.maximized(wp, s, true)
end

---@param s?    Screen
---@param wal?  string
local advanced = function (s, wal)
    return awe.wallpaper {
        screen = s or awe.screen.focused(),

        widget = {
            widget = wibox.container.tile,
            tiled = false,

            {
                widget = wibox.widget.imagebox,
                image = wal or beau.wallpaper,
                resize = true,
                -- upscale = true,
                -- downscale = true,
            },

            valign = 'center',
            halign = 'center',
        },
    }
end

---@param wal string?
local xwallpaper = function (_, wal)
    wal = wal or beau.wallpaper
    if not gea.filesystem.file_readable(wal) then
        -- TODO: notify
        return
    end
    return awe.spawn.with_shell('xwallpaper --zoom ' .. wal)
end

M = {
    basic = basic,
    advanced = advanced,
    xwallpaper = xwallpaper,
}

return M
