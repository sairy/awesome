local topbar = require('ui.bars.topbar')

-- do
--     require('ui.bars.awesome_default')
--     return
-- end

screen.connect_signal('request::desktop_decoration', function (s)
    topbar.setup(s)
end)
