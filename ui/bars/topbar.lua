local awe = require('awful')
local beau = require('beautiful')
local shape = require('gears.shape')
local wibox = require('wibox.init')
local dpi = beau.xresources.apply_dpi

local util = require('util.widget')

local M = {}

local function separator(s, w)
    return wibox.widget.separator {
        orientation = 'vertical',
        span_ratio = w,
        thickness = dpi(2, s),
        forced_width = dpi(5, s),
    }
end

---@param args { screen: Screen, [integer]: { setup: fun(Screen: any) } }
---@return table?
local function mksection(args)
    if #args == 0 then
        return nil
    end

    local s = assert(args.screen)
    for i, widget in ipairs(args) do
        args[i] = widget.setup(s)
    end

    return args
end

--- We don't want padding in monocle mode!
local function monocle_geometry(bar, margins)
    -- for some reason margins can't be queried through bar.margins

    local function eql_keys(t1, t2, ...)
        for _, key in ipairs { ... } do
            if t1[key] ~= t2[key] then
                return false
            end
        end

        return true
    end

    local geometry = bar:geometry()

    local adjustwibar = function (t)
        local scr = t.screen
        local sbar = scr.topwibar

        local new_geo, mrg = geometry, margins

        if t.layout == awe.layout.suit.max then
            mrg = 0
            new_geo = {
                width = scr.geometry.width,
                height = geometry.height,
                x = 0,
                y = 0,
            }
        end

        if not eql_keys(sbar:geometry(), new_geo, 'x', 'y', 'width') then
            sbar.margins = mrg
            sbar:geometry(new_geo)
        end
    end

    tag.connect_signal('property::layout', adjustwibar)
    tag.connect_signal('property::selected', adjustwibar)
end

local setup = function (s)
    local margins = util.margins({
        top = 10,
        left = 20,
        right = 20,
        bottom = 0,
    }, s)

    local bar = awe.wibar {
        bg = beau.bg .. beau.transparency,
        position = 'top',
        height = dpi(26, s),
        screen = s,
        margins = margins,

        -- let picom handle roundness
        shape = shape.rectangle,

        widget = {
            layout = wibox.layout.align.horizontal,
            {
                -- left widgets
                layout = wibox.layout.fixed.horizontal,

                require('ui.widgets.powermenu').setup(),
                require('ui.widgets.tags').setup(s),
                wibox.widget {
                    widget = wibox.container.margin,
                    margins = {
                        left = dpi(6, s),
                        right = dpi(-4, s)
                    },
                    separator(s, 0.8),
                },
                require('ui.widgets.layout').setup(s),

                -- nil terminated due to require() having two return values
                nil,
            },
            require('ui.widgets.tasklist').setup(s), -- middle widget
            mksection {
                -- right widgets
                layout = wibox.layout.fixed.horizontal,
                screen = s,
                spacing = dpi(10, s),
                spacing_widget = separator(s, 0.5),

                require('ui.widgets.net'),
                require('ui.widgets.audio'),
                require('ui.widgets.bat'),
                require('ui.widgets.keyboard'),
                require('ui.widgets.time'),

                -- nil terminated due to require() having two return values
                nil,
            },
        },
    }

    s.topwibar = bar

    awesome.connect_signal('wibox::toggle', function ()
        bar.visible = not bar.visible
    end)

    monocle_geometry(bar, margins)
end

M = {
    setup = setup,
}

return M
