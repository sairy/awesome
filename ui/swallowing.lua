-- The GPLv2 License (GPLv2)

-- Copyright (c) 2023 mira

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-- Sorry BSD users :(
assert(
    os.execute('stat /proc/self >/dev/null 2>&1'),
    'A working procfs under /proc is required for window swallowing'
)

-- Default configuration
local default_config = {
    ---Don't swallow if spawning a floating window
    ignore_floating = false,
    ---The next two configurations are tables of rules
    -- as described by `ruled.client`
    -- Empty tables, {}, will match any window

    ---Swallowing will only take effect if the spawned window
    -- is a child of any client that matches any of these rules
    terminals = {},
    ---Swallowing will not take effect if the window being
    -- spawned matches any of these rules
    excludes = {
        name = { 'Event Tester' }, -- xev
        role = { 'pop-up' },
    },
}

local awe = require('awful')
local match_any = require('ruled').client.match_any
local user_rules = require('beautiful').swallowing or default_config

local M = {}
local enabled = false

---@param pid integer
---@return integer
local function getppid(pid)
    local PID_LINE_NR = 6

    local fp = '/proc/' .. pid .. '/status'
    local f = assert(
        io.open(fp, 'r'),
        string.format('getppid: Failed to open "%s"!', fp)
    )

    for _ = 1, PID_LINE_NR do
        _ = f:read('*l')
    end

    ---@type string
    local line = f:read('*l')
    f:close()

    return tonumber(line:match('PPid:\t(%d+)')) or 0
end

---checks if a process is an ancestor of a client
---@param a_pid integer PID of suposed ancestor
---@param c_pid integer PID of the client
---@return boolean
local function isancestor(a_pid, c_pid)
    repeat
        c_pid = getppid(c_pid)
    until a_pid == c_pid or c_pid < 2

    return c_pid > 1
end

---finds the first client in a screen that matches a filter
---@param filter fun(c: Client):boolean filter function
---@param s?     Screen The screen to search in; defaults to `awful.screen.focused`
---@return Client? # first client to match the filter
local function findclient(filter, s)
    local clients = client.get(s or awe.screen.focused())

    for _, c in ipairs(clients) do
        if filter(c) then
            return c
        end
    end
end

---@param c Client
local swallow = function (c)
    if
        match_any(c, user_rules.excludes)
        or (c.floating and user_rules.ignore_floating)
    then
        return
    end

    local ancestor = findclient(function (cl)
        -- order matters; isancestor() is left last to avoid unnecessary loops
        return cl ~= c
            and match_any(cl, user_rules.terminals)
            and isancestor(cl.pid, c.pid)
    end)

    if ancestor then
        c:tags(ancestor:tags())
        c:swap(ancestor)
        ancestor.hidden = true
        c.swallowed = ancestor
    end
end

---@param c Client
local unswallow = function (c)
    if c.swallowed then
        --TODO: swap back the clients? c:swap() crashes awesome
        c.swallowed.hidden = false
        c.swallowed:activate { context = 'unswallow', raise = true }
        c.swallowed = nil
    end
end

---Enable window swallowing
local enable = function ()
    client.connect_signal('request::manage', swallow)
    client.connect_signal('request::unmanage', unswallow)

    for _, c in ipairs(client.get()) do
        swallow(c)
    end

    enabled = true
end

---Disable window swallowing
local disable = function ()
    client.disconnect_signal('request::manage', swallow)
    client.disconnect_signal('request::unmanage', unswallow)

    for _, c in ipairs(client.get()) do
        unswallow(c)
    end

    enabled = false
end

---Toggle window swallowing on/off
local toggle = function ()
    if enabled then
        return disable()
    else
        return enable()
    end
end

M = {
    enable = enable,
    disable = disable,
    toggle = toggle,
    is_active = function ()
        return enabled
    end,
}

return M
