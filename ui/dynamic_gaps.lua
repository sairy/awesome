local awe = require('awful')
local beau = require('beautiful')

local M = {}

if not beau.gaps then
    beau.gaps = {
        backup = 0,
        change_amt = 1,
    }
end

local GAPS_MIN = 0
local GAPS_MAX = 150

local last_val = beau.gaps.backup or 0

local function clamp_gap(x)
    return math.clamp(x, GAPS_MIN, GAPS_MAX)
end

local function changeto(x)
    last_val = beau.useless_gap
    beau.useless_gap = clamp_gap(x)

    local s = awe.screen.focused()

    tag.emit_signal('property::layout', s.selected_tag)
    return awe.layout.arrange(s)
end

---Sets the gaps to the given value
---Limited to values between GAPS_MIN and GAPS_MAX
---@param size number
local gaps_set = function (size)
    local new_size = tonumber(size)

    if not new_size then
        return
    end

    return changeto(new_size)
end

---Increment gaps by the given value
---@param amt number? defaults to beautiful.gaps.change_amt
local gaps_incf = function (amt)
    amt = amt or beau.gaps.change_amt
    return changeto(beau.useless_gap + amt)
end

---Shorthand for decf(-amt)
---@param amt number? defaults to -beautiful.gaps.change_amt
local gaps_decf = function (amt)
    amt = amt or -beau.gaps.change_amt
    return changeto(beau.useless_gap + amt)
end

---Reset gaps to the value of beautiful.gaps.backup
local gaps_reset = function ()
    return changeto(beau.gaps.backup)
end

local ON = beau.useless_gap ~= 0
---Toggle gaps between zero and beautiful.gaps.backup
local gaps_toggle = function ()
    ON = not ON
    return changeto(ON and last_val or 0)
end

M = {
    set = gaps_set,
    incf = gaps_incf,
    decf = gaps_decf,
    toggle = gaps_toggle,
    reset = gaps_reset,
    MIN = GAPS_MIN,
    MAX = GAPS_MAX,
}

return M
