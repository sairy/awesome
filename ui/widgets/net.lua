local awe = require('awful')
local beau = require('beautiful')
local gears = require('gears')

local net, term_cmd
do
    local conf = require('conf.default')
    net = conf.net
    term_cmd = conf.term
end

local util = require('util.widget')
local fmt = require('util.unit_fmt').fmt

---@type text_element
local icon_def = {
    fg = '#f38ba8',
    text = net.icon,
    font = beau.font_mono,
}

local val_def = { text = '!404!', fg = '#f38ba8' }

local M = {}

local function checkwhich()
    local path = function (intf)
        return net.pfx .. intf .. '/operstate'
    end

    for _, intf in ipairs { net.ether, net.wifi } do
        local f = io.open(path(intf), 'r')

        if f then
            local line = f:read('l')
            f:close()

            if line == 'up' then
                return intf
            end
        end
    end

    return nil
end

---@type number
local Old_bytes
local function update(widget)
    local intf = checkwhich()
    local text

    if not intf then
        return
    end

    local f = io.open(net.pfx .. intf .. net[net.which], 'r')
    if not f then
        return
    end

    ---@type number
    local val = assert(f:read('n'), 'net_' .. tostring(net.which) .. ': not a number!')
    f:close()

    if Old_bytes then
        local speed = (val - Old_bytes) / net.timeout
        text = fmt(speed) .. 'B/s'
    end

    Old_bytes = val

    util.update_text(widget, text or val_def.text)
end

local buttons = {
    awe.button({}, 1, function ()
        awe.spawn.with_shell(net.manager)
    end),
    awe.button({}, 3, function ()
        awe.spawn.with_shell(string.format('%s -e nmtui', term_cmd))
    end),
}

M.setup = function ()
    local widget = util.base_widget(icon_def, val_def, { clickable = true })

    widget:buttons(buttons)
    widget._timer = gears.timer {
        timeout = net.timeout,
        call_now = true,
        autostart = true,
        callback = function ()
            update(widget)
        end,
    }
    return widget
end

return M
