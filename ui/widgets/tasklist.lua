local awe = require('awful')
local wibox = require('wibox.init')
local dpi = require('beautiful.xresources').apply_dpi

local util = require('util.widget')

local M = {}

M.setup = function (s)
    local widget = awe.widget.tasklist {
        screen = s,
        filter = awe.widget.tasklist.filter.focused,
        buttons = {
            awe.button({}, 1, function (c)
                c:activate { context = 'tasklist', action = 'toggle_minimization' }
            end),
            -- awe.button({}, 3, function ()
            --     awe.menu.client_list { theme = { width = 250 } }
            -- end),
            awe.button({}, 6, function ()
                awe.client.focus.byidx(1)
            end),
            awe.button({}, 7, function ()
                awe.client.focus.byidx(-1)
            end),
        },
        widget_template = {
            id = 'background_role',
            widget = wibox.container.background,
            halign = 'center',
            valign = 'center',
            {
                widget = wibox.container.margin,
                margins = util.margins {
                    left = 10,
                    right = 10,
                },
                {
                    layout = wibox.layout.align.horizontal,
                    expand = 'outside',
                    [2] = {
                        layout = wibox.layout.fixed.horizontal,
                        spacing = dpi(4),
                        {
                            widget = wibox.container.margin,
                            margins = util.margins {
                                top = 2,
                                bottom = 2,
                                right = 2,
                            },
                            {
                                id = 'icon_role',
                                resize = true,
                                valign = 'center',
                                halign = 'center',
                                widget = wibox.widget.imagebox,
                            },
                        },
                        {
                            id = 'text_role',
                            widget = wibox.widget.textbox,
                        },
                    },
                },
            },
        },
    }

    return widget
end

return M
