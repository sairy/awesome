local awe = require('awful')
local beau = require('beautiful')
local shape = require('gears.shape')
local wibox = require('wibox.init')
local dpi = beau.xresources.apply_dpi

local util = require('util.widget')

local M = {}

local mod = 'Mod4'

local buttons = {
    awe.button({}, 1, awe.tag.viewonly),
    awe.button({ mod }, 1, function (t)
        if client.focus then
            client.focus:move_to_tag(t)
        end
    end),
    awe.button({}, 3, awe.tag.viewtoggle),
    awe.button({ mod }, 3, function (t)
        if client.focus then
            client.focus:toggle_tag(t)
        end
    end),
}

M.setup = function (s)
    local widget = awe.widget.taglist {
        screen = s,
        filter = awe.widget.taglist.filter.all,
        buttons = buttons,
        style = {
            shape = shape.rectangle,
            font = beau.font_kanji:as_str(),
        },
        layout = {
            spacing = dpi(1, s),
            layout = wibox.layout.fixed.horizontal,
        },
        widget_template = {
            widget = wibox.container.background,
            id = 'background_role',
            -- fg = beau.pink,
            {
                widget = wibox.container.margin,
                {
                    layout = wibox.layout.fixed.horizontal,
                    {
                        widget = wibox.widget.textbox,
                        id = 'text_role',
                    },
                },
                margins = util.margins({
                    -- top = 1,
                    -- bottom = 7,
                    left = 10,
                    right = 10,
                }, s),
            },
        },
    }

    require('util.widget').hover_effect.cursor(widget)

    s.__tags = widget
    return widget
end

return M
