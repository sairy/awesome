local awe = require('awful')
local wibox = require('wibox.init')

local kb = require('util.keyboard')
local util = require('util.widget')

local M = {}

local buttons = {
    awe.button({}, 1, kb.next),
    awe.button({}, 3, kb.prev),
}

M.setup = function ()
    local widget = wibox.widget {
        widget = wibox.container.background,
        {
            widget = wibox.container.margin,
            margins = util.margins {
                left = 10,
            },
            {
                layout = wibox.layout.fixed.horizontal,
                {
                    widget = wibox.widget.textbox,
                    markup = string.format(
                        '<span foreground="%s">%s</span>',
                        '#89dceb',
                        ''
                    ),
                },
                {
                    widget = wibox.container.background,
                    fg = '#89dceb',
                    awe.widget.keyboardlayout(),
                },
            },
        },
    }

    require('util.widget').hover_effect.cursor(widget)
    widget:buttons(buttons)
    return widget
end

return M
