local awe = require('awful')
local beau = require('beautiful')
local wibox = require('wibox.init')

local util = require('util.widget')
local markup = require('util.markup')

local cfg = require('conf.default').bar_powermenu

local buttons = {
    awe.button({}, 1, function ()
        awesome.emit_signal('prompt::powermenu::show')
    end),
}

local M = {}

M.setup = function ()
    local widget = wibox.widget {
        widget = wibox.container.background,
        {
            widget = wibox.container.margin,
            margins = util.margins {
                left = 10,
                right = 5,
            },
            {
                layout = wibox.layout.fixed.horizontal,
                {
                    widget = wibox.widget.textbox,
                    markup = markup.fontfg {
                        font = beau.font_nerd,
                        text = cfg.icon,
                    },
                    halign = 'center',
                    valign = 'center',
                },
            },
        },
    }

    util.hover_effect.cursor(widget)
    widget:buttons(buttons)
    return widget
end

return M
