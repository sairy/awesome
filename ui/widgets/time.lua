local beau = require('beautiful')
local wibox = require('wibox.init')

local markup = require('util.markup')
local util = require('util.widget')

local M = {}

M.setup = function ()
    -- local widget = util.base_widget(icon, text)

    -- widget._timer = timer.new {
    --     timeout = 15,
    --     call_now = true,
    --     autostart = true,
    --     callback = function ()
    --         local fmt = '%B%d日 (%a) %H:%M'
    --         text.text = os.date(fmt) --[[@as string]]
    --         util.update_markup(widget, nil, text)
    --     end,
    -- }

    local widget = wibox.widget {
        widget = wibox.container.background,
        {
            widget = wibox.container.margin,
            margins = util.margins {
                left = 10,
                right = 10,
            },
            {
                layout = wibox.layout.fixed.horizontal,
                {
                    widget = wibox.widget.textbox,
                    markup = markup.fontfg {
                        font = beau.font_nerd,
                        fg = '#cba6f7',
                        text = '󰃭 ',
                    },
                },
                {
                    widget = wibox.container.background,
                    fg = '#cba6f7',
                    {
                        widget = wibox.widget.textclock,
                        format = '%B%d日（%a）%H:%M',
                        font = beau.font_kanji:as_str(),
                    },
                },
            },
        },
    }

    return widget
end

return M
