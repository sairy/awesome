local awe = require('awful')
local beau = require('beautiful')
local gears = require('gears')

local util = require('util.widget')
local cfg = require('conf.default').bat

--TODO: remove assertions

local icons = cfg.icons

---@type text_element
local icon_def = {
    fg = '#a6e3a1',
    text = icons.empty,
    font = beau.font_nerd,
}

local val_def = { text = 'n/a', fg = '#a6e3a1' }

local M = {}

---@param timer Timer?
---@param new_timeout integer
local function reset_timeout(timer, new_timeout)
    if not timer or timer.timeout == new_timeout then
        return
    end

    timer:stop()
    timer.timeout = new_timeout
    timer:start()
end

local function checkwhich()
    local file = 'online'

    local f = assert(io.open(cfg.charger:path() .. file, 'r'))
    local charging = f:read('l') == '1'
    f:close()

    return charging and cfg.charger or cfg.battery
end

local At = 1
---@param level integer
---@return string
local choose_icon = function (bat, level)
    local icon

    if bat == cfg.battery then
        if level < 11 then
            icon = icons.empty
        elseif level < 75 then
            icon = icons.half
        elseif level < 98 then
            icon = icons.almost
        else
            icon = icons.full
        end
    else
        -- if charging, make it an animation
        icon = icons.animation[At]
        At = (At % #icons.animation) + 1
    end

    return icon
end

---@return integer
local function readlevel()
    local f = assert(io.open(cfg.battery:path() .. 'capacity', 'r'))
    local level = f:read('l')
    f:close()

    return assert(tonumber(level))
end

---@return string # time remaining in 'Xh Ym'
local function bat_rem(bat)
    ---@param f1 string
    ---@param f2 string
    ---@return integer
    local pick_and_read = function (f1, f2)
        local f = io.open(bat:path() .. f1, 'r')
            or assert(io.open(bat:path() .. f2, 'r'))

        local value = f:read('n')
        f:close()

        return assert(tonumber(value))
    end

    local charge = pick_and_read('energy_now', 'charge_now')
    local current = pick_and_read('power_now', 'current_now')

    local time = charge / current
    local hr, min = math.modf(time)
    min = math.floor(min * 60)

    return string.format('%dh %dm', hr, min)
end

---@type ('perc'|'rem')?
local mode = nil

local function update(widget)
    local bat = checkwhich()
    local level = readlevel()

    mode = mode or bat.show

    local icon = choose_icon(bat, level)
    local text = (bat == cfg.charger or mode == 'perc')
        and string.format('%d%%', level)
        or bat_rem(bat)

    util.update_markup(widget, icon, text)
    reset_timeout(widget._timer, bat.timeout)
end

M.setup = function ()
    local widget = util.base_widget(icon_def, val_def, { clickable = true })

    widget:buttons {
        awe.button({}, 1, function ()
            if mode == 'rem' then
                mode = 'perc'
            else
                mode = 'rem'
            end
            update(widget)
        end),
    }

    widget._timer = gears.timer {
        timeout = cfg.battery.timeout,
        call_now = true,
        autostart = true,
        callback = function ()
            return update(widget)
        end,
    }

    return widget
end

return M
