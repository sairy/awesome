local awe = require('awful')
local wibox = require('wibox.init')
local dpi = require('beautiful.xresources').apply_dpi

local util = require('util.widget')

local M = {}

local buttons = {
    awe.button({}, 1, function () return awe.layout.inc(1) end),
    awe.button({}, 3, function () return awe.layout.inc(-1) end),
    awe.button({}, 6, function () return awe.layout.inc(1) end),
    awe.button({}, 7, function () return awe.layout.inc(-1) end),
}

M.setup = function (s)
    local widget = wibox.widget {
        widget = wibox.container.background,
        -- bg = beau.bg,
        {
            widget = wibox.container.margin,
            margins = util.margins {
                left = dpi(8),
                right = dpi(8),
            },
            {
                layout = wibox.layout.fixed.horizontal,
                awe.widget.layoutbox {
                    screen = s,
                    buttons = buttons,
                },
            },
        },
    }

    require('util.widget').hover_effect.cursor(widget)
    return widget
end

return M
