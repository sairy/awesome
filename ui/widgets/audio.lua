local awe = require('awful')
local beau = require('beautiful')

local audio = require('env.audio')
local icons = require('conf.default').audio.icons
local util = require('util.widget')

local M = {}


---@type text_element
local icon_def = {
    fg = '#fab387',
    text = icons.muted,
    font = beau.font_nerd,
}

---@type text_element
local text_def = { text = 'n/a', fg = '#fab387' }

local update = function (widget)
    awe.spawn.easy_async_with_shell(audio:volume(), function (stdout)
        local vol = assert(tonumber(stdout), 'volume % was not a number!')

        local icon
        if vol == 0 then
            icon = icons.muted
        elseif vol < 50 then
            icon = icons.quiet
        else
            icon = icons.loud
        end

        util.update_markup(widget, icon, string.format('%d%%', vol))
    end)
end


local buttons = {
    awe.button({}, 1, function ()
        awe.spawn.with_line_callback(audio:tog(), {
            exit = function ()
                awesome.emit_signal('audio::changed', 'tog')
            end,
        })
    end),
    awe.button({}, 4, function ()
        awe.spawn.with_line_callback(audio:dec(), {
            exit = function ()
                awesome.emit_signal('audio::changed', 'dec')
            end,
        })
    end),
    awe.button({}, 5, function ()
        awe.spawn.with_line_callback(audio:inc(), {
            exit = function ()
                awesome.emit_signal('audio::changed', 'inc')
            end,
        })
    end),
}

M.setup = function ()
    local widget = util.base_widget(icon_def, text_def, { clickable = true })

    widget:buttons(buttons)

    ---@param how Audio.how
    awesome.connect_signal('audio::changed', function (how)
        if how ~= 'tog' then
            update(widget)
        else
            ---@param stdout string
            awe.spawn.easy_async_with_shell(audio:muted(), function (stdout)
                if stdout:match('true') then
                    util.update_markup(widget, icons.muted, 'muted')
                else
                    update(widget)
                end
            end)
        end
    end)

    return widget
end

return M
