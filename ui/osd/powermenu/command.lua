local awe = require('awful')

local M = {}

M.hide_signal = function ()
    awesome.emit_signal('prompt::powermenu::hide')
end

M.show_signal = function ()
    awesome.emit_signal('prompt::powermenu::show')
end

---@param cmd string|fun()
---@return fun()
M.gen_cmd = function (cmd)
    return type(cmd) == 'function'
        and cmd
        or function ()
            awe.spawn.with_shell(cmd)
            M.hide_signal()
        end
end

return M
