local awe = require('awful')
local beau = require('beautiful')
local gea = require('gears')
local wibox = require('wibox.init')

local util = require('util.widget')

---@class powermenu.cmd
---@field icon string
---@field label string
---@field comm fun()

local M = {}

---@param commands  powermenu.cmd[] commands
---@param s         Screen
---@param fonts     { text: Font, icon: Font }?
---@return Object[] # buttons
M.create = function (commands, s, fonts)
    fonts = gea.table.crush(
        { text = beau.fnt, icon = beau.font_nerd:with_size(14) },
        fonts or {}
    ) --[[@as { text: Font, icon: Font }]]

    local t = {}
    for _, cmd in ipairs(commands) do
        local widget = util.base_widget(
            { font = fonts.icon, text = cmd.icon },
            { font = fonts.text, text = cmd.label }
        )

        local outside = wibox.widget {
            layout = wibox.layout.align.vertical,
            spacing = 0,
            {
                widget = wibox.container.background,
                id = 'background',
                shape = gea.shape.rounded_rect,
                {
                    widget = wibox.container.margin,
                    margins = util.margins({
                        top = 5,
                        bottom = 5,
                        left = 0,
                        right = 0,
                    }, s),
                    widget,
                },
            },
        }

        outside.buttons = { awe.button({}, 1, cmd.comm) }
        util.hover_effect.cursor(outside)
        util.hover_effect.color(
            outside,
            { bg = beau.gray2 .. beau.transparency }
        )

        t[#t+1] = outside
    end

    return t
end

return M
