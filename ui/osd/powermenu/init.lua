local beau = require('beautiful')
local gtable = require('gears.table')
local rubato = require('lib.rubato')
local wibox = require('wibox.init')

local widgets = require('ui.osd.powermenu.widgets')
local command = require('ui.osd.powermenu.command')
local btn_util = require('ui.osd.powermenu.buttons')
local keyhandle = require('ui.osd.powermenu.keyhandle')

local dpi = beau.xresources.apply_dpi

local user_conf = require('conf.default').osd.powermenu

---@class powermenu.config.default_action
---@field timeout integer
---@field comm string
---@field phrase string

---@class powermenu.config
---@field username (string|fun():string) username to display
---@field avatar (string|fun():string) path to user avatar image
---@field screenlocker string screen lock application name/path
---@field font Font font used to render the menu; icons still rendered by beau.font_nerd
---@field default_action? powermenu.config.default_action

---@type powermenu.config
local default = {
    username = function ()
        return os.getenv('USER') or 'anon'
    end,
    avatar = function ()
        local gfs = require('gears.filesystem')
        local path = '/var/lib/AccountsService/icons/' .. os.getenv('USER')

        return gfs.file_readable(path)
            and path
            or gfs.get_configuration_dir() .. 'assets/avi'
    end,
    screenlocker = '',
    font = beau.fnt:with_size(22),
    default_action = {
        timeout = 10,
        comm = '/sbin/poweroff',
        phrase = 'Shutting down',
    },
}

---@type powermenu.config
local config = gtable.crush(default, user_conf or {})

---@type powermenu.cmd[]
local commands = {
    {
        icon = ' ',
        label = 'Shutdown',
        comm = command.gen_cmd('/sbin/poweroff'),
    },
    {
        icon = ' ',
        label = 'Reboot',
        comm = command.gen_cmd('/sbin/reboot'),
    },
    {
        icon = ' ',
        label = 'Suspend',
        comm = command.gen_cmd(config.screenlocker .. ' & systemctl suspend &'),
    },
    {
        icon = ' ',
        label = 'Lock',
        comm = command.gen_cmd(config.screenlocker .. ' &'),
    },
    {
        icon = ' ',
        label = 'Log Out',
        comm = function ()
            awesome.quit()
        end,
    },
}

---TODO: multiscreen support

local buttons = btn_util.create(commands, screen[1], { text = config.font })
local w = widgets.create(config)
local grabber = keyhandle.grabber(buttons, w.countdown, config.font)

local menu = wibox.widget {
    layout = wibox.layout.align.vertical,
    expand = 'outside',

    [2] = {
        layout = wibox.layout.align.horizontal,
        expand = 'outside',

        [2] = {
            layout = wibox.layout.fixed.vertical,
            spacing = dpi(15),

            {
                layout = wibox.layout.fixed.vertical,
                spacing = dpi(5),

                {
                    widget = wibox.container.margin,
                    margins = dpi(10),
                    w.avi,
                },

                {
                    widget = wibox.container.margin,
                    margins = dpi(5),
                    w.name,
                },
            },

            {
                layout = wibox.layout.fixed.vertical,
                spacing = dpi(10),

                {
                    widget = wibox.container.margin,
                    margins = dpi(5),
                    {
                        layout = wibox.layout.fixed.horizontal,
                        spacing = dpi(10),
                        table.unpack(buttons),
                    },
                },
                {
                    widget = wibox.container.margin,
                    margins = dpi(5),
                    w.countdown,
                },
            },
        },
    },
}

local box = wibox {
    screen = screen[1],
    type = 'splash',
    ontop = true,
    visible = false,

    bg = beau.bg .. beau.transparency,
    opacity = 0,

    width = screen[1].geometry.width,
    height = screen[1].geometry.height,
    x = screen[1].geometry.x,
    y = screen[1].geometry.y,

    widget = menu,
}

local fade = rubato.timed {
    duration = 0.50,
    awestore_compat = true,
}

fade:subscribe(function (opacity)
    box:set_opacity(opacity)
end)

fade.started:subscribe(function ()
    if fade.target >= 0 then
        box.visible = true
    end
end)

fade.ended:subscribe(function ()
    if fade.target == 0 then
        box.visible = false
    end
end)

awesome.connect_signal('prompt::powermenu::show', function ()
    fade.target = 1
    w.countdown:update(config.default_action.timeout)
    w.countdown.timer:start()
    grabber:start()
end)

awesome.connect_signal('prompt::powermenu::hide', function ()
    w.countdown.timer:stop()
    fade.target = 0
    grabber:stop()
end)
