local beau = require('beautiful')
local gea = require('gears')
local wibox = require('wibox.init')

local command = require('ui.osd.powermenu.command')
local markup = require('util.markup')

---@param f_str string|fun():string
---@return string
local function fun_or_str(f_str)
    return type(f_str) == 'function'
        and f_str()
        or f_str --[[@as string]]
end

local M = {}

---@param config powermenu.config
M.create = function (config)
    local t = {}

    local avi = fun_or_str(config.avatar)
    local name = fun_or_str(config.username)

    t.avi = wibox.widget {
        widget = wibox.widget.imagebox,
        image = avi,
        shape = gea.shape.circle,

        resize = false,
        downscale = false,
        upscale = false,

        halign = 'center',
        valign = 'center',
    }

    t.name = wibox.widget {
        widget = wibox.widget.textbox,
        markup = markup.fontfg {
            text = name,
            font = config.font
        },

        halign = 'center',
        valign = 'center',
    }

    local countdown = wibox.widget {
        widget = wibox.widget.textbox,
        font = beau.fnt:as_str(),
        markup = markup.fontfg {
            text = 'this should not show up',
            font = config.font
        },

        halign = 'center',
        valign = 'center',
    }

    countdown.update = function (self, remaining)
        local text = string.format(
            '%s in %d second%s',
            config.default_action.phrase,
            remaining,
            remaining ~= 1 and 's' or '' -- pluralize message
        )

        self.markup = markup.fontfg {
            text = text,
            font = config.font
        }
        self.timeout = remaining - 1
    end

    local comm = command.gen_cmd(config.default_action.comm)
    countdown.timer = gea.timer {
        timeout = 1,
        call_now = false,
        autostart = false,

        callback = function ()
            local remaining = countdown.timeout
            if remaining > 0 then
                countdown:update(remaining)
            else
                comm()
            end
        end,
    }

    t.countdown = countdown

    return t
end

return M
