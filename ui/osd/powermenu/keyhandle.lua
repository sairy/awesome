local awe = require('awful')
local beau = require('beautiful')
local gmath = require('gears.math')
local gtable = require('gears.table')

local markup = require('util.markup')
local command = require('ui.osd.powermenu.command')

---@class KeyGrabber
---@field start fun(self)
---@field stop fun(self)

local PAUSE_KEY = 'F1'

local tab_at = 0
---@type Object?
local last_hl

local function hint_gc()
    if last_hl then
        last_hl:emit_signal('mouse::leave')
    end
    last_hl = nil
end

local M = {}

---@param buttons Object[] buttons
---@param countdown table countdown widget
---@param _font Font
---@return KeyGrabber
M.grabber = function (buttons, countdown, _font)
    local grabber
    grabber = awe.keygrabber {
        autostart = false,
        stop_event = 'release',

        ---@param _ any
        ---@param mods string[]
        ---@param key string
        keypressed_callback = function (_, mods, key)
            if key == 'Escape' then
                command.hide_signal()
                hint_gc()
            elseif key == 'Return' and buttons[tab_at] then
                ---@diagnostic disable-next-line:undefined-field
                buttons[tab_at].buttons[1]:trigger()
                hint_gc()
            elseif key == PAUSE_KEY then
                local timer = countdown.timer
                if timer.started then
                    timer:stop()
                    grabber.old_text = countdown.markup
                    countdown.markup = markup.fontfg {
                        fg = beau.fg,
                        text = 'Countdown Paused',
                        font = _font,
                    }
                    hint_gc()
                else
                    countdown.markup = grabber.old_text
                    timer:start()
                end
            elseif key == 'Tab' then
                -- cycle backwards if shift was held
                if gtable.hasitem(mods, 'Shift') then
                    tab_at = tab_at == 0 and tab_at or tab_at - 1
                    tab_at = gmath.cycle(#buttons, tab_at) --[[@as integer]]
                else
                    tab_at = (tab_at % #buttons) + 1
                end

                if last_hl then
                    last_hl:emit_signal('mouse::leave')
                end
                buttons[tab_at]:emit_signal('mouse::enter')

                last_hl = buttons[tab_at]
            end
        end,
    }

    return grabber
end

return M
