local cairo = require('lgi').cairo
local gcolor = require('gears.color')

local M = {}

---@generic S:  table       cairo image surface
---@param size  integer     square size
---@param fg    string      square foreground
---@param x     integer?    defaults to 0
---@param y     integer?    defaults to 0
---@return S
---@see beautiful.theme_assets.taglist_squares_sel
M.taglist_squares_sel = function (size, fg, x, y)
    x, y = x or 0, y or 0

    local img = cairo.ImageSurface(cairo.Format.ARGB32, size + x, size + y)
    local cr = cairo.Context(img)

    cr:set_source(gcolor.create_pattern(fg))
    cr:rectangle(x, y, size, size)
    cr:fill()

    return img
end

---@generic S:          table       cairo image surface
---@param size          integer     square size
---@param fg            string      square border color
---@param x             integer?    defaults to 0
---@param y             integer?    defaults to 0
---@param line_width    integer?    defaults to 1
---@return S
---@see beautiful.theme_assets.taglist_squares_unsel
M.taglist_squares_unsel = function (size, fg, x, y, line_width)
    x, y, line_width = x or 0, y or 0, line_width or 1

    local img = cairo.ImageSurface(cairo.Format.ARGB32, size + x, size + y)
    local cr = cairo.Context(img)
    local lw2 = line_width / 2

    cr:set_source(gcolor.create_pattern(fg))
    cr:set_line_width(size / 4)
    cr:rectangle(x + lw2, y + lw2, size - lw2, size - lw2)
    cr:stroke()

    return img
end

return M
