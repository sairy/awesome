require('extensions.math')
require('extensions.table')

do
    local ok, ins = pcall(require, 'inspect')

    if ok and not awesome.inspect then
        -- this line doesn't do anything due to __newindex
        -- but it helps provide LSP completions

        awesome.inspect = function (root, options) return ins.inspect(root, options) end

        rawset(awesome, 'inspect', ins.inspect)
    end
end
