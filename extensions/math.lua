---Clamps `x` betweeen `min` and `max`
---@generic T: number
---@param x T
---@param min T
---@param max T
---@return T
math.clamp = function (x, min, max)
    return math.min(max, math.max(min, x))
end

---Rounds `x` to the nearest integer
---@generic T: number
---@param x T
---@return integer
math.round = function (x)
    return math.floor(x + 0.5)
end
