---@version <5.2, JIT
if _VERSION < 'Lua 5.2' then
    --These lua versions do not have `table.pack`,
    --and `unpack` is outside the table lib.
    --Therefore, we adjust the environment
    --to conform to Lua >= 5.2.

    --Lua_LS raises diagnostic warnings anyways
    ---@diagnostic disable-next-line: deprecated
    table.unpack = unpack

    ---@param ... any
    ---@return table
    ---@nodiscard
    ---@diagnostic disable-next-line: duplicate-set-field
    table.pack = function (...)
        local t = { ... }
        t.n = #t
        return t
    end
end
