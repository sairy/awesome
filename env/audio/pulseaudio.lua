---@class AudioTool.Pulse: AudioTool
local Pulse = {}

local cmd = 'pamixer'

function Pulse:volume(source)
    local which = source
        and ' --source ' .. self.props.source
        or ' --sink ' .. self.props.sink

    return cmd .. which .. ' --get-volume'
end

function Pulse:muted(source)
    local which = source
        and ' --source ' .. self.props.source
        or ' --sink ' .. self.props.sink

    return cmd .. which .. ' --get-mute'
end

function Pulse:change(how, amt, source)
    local flag, _arg = 'sink', self.props.sink
    if source then
        flag, _arg = 'source', self.props.source
    end

    if how ~= 'tog' then
        return string.format(
            '%s --%s %s -%s %d',
            cmd,
            flag,
            _arg,
            how:sub(1, 1),
            amt or self.props.step
        )
    else
        return string.format('%s --%s %s -t', cmd, flag, _arg)
    end
end

function Pulse:inc(amt, source)
    return self:change('inc', amt, source)
end

function Pulse:dec(amt, source)
    return self:change('dec', amt, source)
end

function Pulse:tog(source)
    return self:change('tog', nil, source)
end

return Pulse
