local injector = require('env.injector')
local cfg = require('conf.default').audio

---@alias Audio.how 'inc'|'dec'|'tog'

---@class Audio.props
---@field step   integer
---@field source string # microphone
---@field sink   string # speaker/headset

--{{{ EmmyLua
---@class AudioTool
---@field protected props Audio.props
local Dummy = {}

---@diagnostic disable: unused-local

---@param source boolean? source or sink volume
---@return string #string suitable for awful.spawn.easy_async_with_shell
function Dummy:volume(source) return '' end

---@param source boolean? source or sink volume
---@return string #string suitable for awful.spawn.easy_async_with_shell
function Dummy:muted(source) return '' end

---@param how Audio.how
---@param amt integer?
---@param source boolean?
function Dummy:change(how, amt, source) return '' end

---@param amt integer?
---@param source boolean?
function Dummy:inc(amt, source) return '' end

---@param amt integer?
---@param source boolean?
function Dummy:dec(amt, source) return '' end

---@param source boolean?
function Dummy:tog(source) return '' end

---@diagnostic enable: unused-local
---}}}

Dummy.props = cfg.props

---@type AudioTool
local audio = injector.new {
    lookup = {
        pulseaudio = 'env.audio.pulseaudio',
    },
    impl = cfg.backend,
    index = Dummy,
}

return audio
