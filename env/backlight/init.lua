local injector = require('env.injector')
local cfg = require('conf.default').brightness

---@class Backlight.props
---@field step integer

---for EmmyLua Annotations
---@class BacklightTool
---@field protected props Backlight.props
local Dummy = {}

---@diagnostic disable: unused-local
---@param step integer?
function Dummy:inc(step) return '' end

---@param step integer?
function Dummy:dec(step) return '' end

---@diagnostic enable: unused-local

Dummy.props = cfg.props

---@type BacklightTool
local backlight = injector.new {
    lookup = {
        light = 'env.backlight.light',
    },
    impl = cfg.backend,
    index = Dummy,
}

return backlight
