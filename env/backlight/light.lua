---@class BacklightTool.Light: BacklightTool
local Light = {}

local cmd = 'light'

function Light:inc(step)
    step = step or self.props.step
    return string.format('%s -A %s', cmd, step)
end

function Light:dec(step)
    step = step or self.props.step
    return string.format('%s -U %s', cmd, step)
end

return Light
