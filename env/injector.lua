local M = {}

---@class Injector.config
---@field lookup table<string, string> map<implname, req_path>
---@field impl string name
---@field index table

---@param config Injector.config
---@return table
M.new = function (config)
    local req_path = assert(
        config.lookup[config.impl],
        'Injector: unknown impl: ' .. config.impl
    )

    local impl = require(req_path)
    setmetatable(impl, {
        __index = config.index
    })

    return impl
end

return M
