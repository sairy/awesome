local injector = require('env.injector')
local cfg = require('conf.default').screenshot

---@class ScreenshotTool
local Dummy = {}

function Dummy:full() return '' end

function Dummy:screen() return '' end

function Dummy:choose_area() return '' end

---@type ScreenshotTool
local scr = injector.new {
    lookup = {
        flameshot = 'env.screenshot.flameshot',
    },
    impl = cfg.backend,
    index = Dummy,
}

return scr
