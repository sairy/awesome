---@class ScreenshotTool.Flameshot: ScreenshotTool
local Flameshot = {}

local cmd = 'flameshot'

function Flameshot:full()
    return cmd .. ' full'
end

function Flameshot:screen()
    return cmd .. ' screen'
end

function Flameshot:choose_area()
    return cmd .. ' gui'
end

return Flameshot
