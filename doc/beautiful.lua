---@meta

---TODO:
---@class lgi.Pango.FontDescription

---@type Beautiful.theme
local theme = {}

---@class Beautiful
---@field private mt metatable
---@field private theme_path string?
local Beautiful = {
    ---@module 'beautiful.xresources'
    xresources = {},

    ---@module 'beautiful.theme_assets'
    theme_assets = {},

    ---@module 'beautiful.gtk'
    gtk = {},
}

Beautiful.mt = {}

function Beautiful.mt:__index(k)
    return theme[k]
end

function Beautiful.mt:__newindex(k, v)
    theme[k] = v
end


--- Function that initializes the theme settings. Should be run at the
-- beginning of the awesome configuration file (normally rc.lua).
--
-- Example usages:
--```lua
--    -- Using a table
--    beautiful.init({font = 'Monospace Bold 10'})
--
--    -- From a config file
--    beautiful.init("<path>/theme.lua")
--```
-- Example "<path>/theme.lua" (see `05-awesomerc.md:Variable_definitions`):
--```lua
--    theme = {}
--        theme.font = 'Monospace Bold 10'
--    return theme
--```
-- Example using the return value:
--```lua
--    local beautiful = require("beautiful")
--    if not beautiful.init("<path>/theme.lua") then
--        beautiful.init("<path>/.last.theme.lua") -- a known good fallback
--    end
--```
---@param config string|Beautiful.theme The theme to load.
-- It can be either the path to the theme file (which should return a table)
-- or directly a table containing all the theme values.
---@return true? #True if successful, nil in case of error.
function Beautiful.init(config) end

--- Get the current theme.
---@return Beautiful.theme #The current theme table.
function Beautiful.get() end

--- Get a font description.
--
-- See https://developer.gnome.org/pango/stable/pango-Fonts.html#PangoFontDescription.
---@param name string|lgi.Pango.FontDescription The name of the font.
---@return lgi.Pango.FontDescription
function Beautiful.get_font(name) end

--- Get a new font with merged attributes, based on another one.
--
-- See https://developer.gnome.org/pango/stable/pango-Fonts.html#pango-font-description-from-string.
---@param name string|lgi.Pango.FontDescription The base font.
---@param merge string Attributes that should be merged, e.g. "bold".
---@treturn lgi.Pango.FontDescription
function Beautiful.get_merged_font(name, merge) end

--- Get the height of a font.
--
---@param name string Name of the font.
---@return number #The font height.
function Beautiful.get_font_height(name) end

return setmetatable(Beautiful, Beautiful.mt)
