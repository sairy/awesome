---@meta

---@class Mouse
---@field screen? Screen The screen under the cursor
---@field current_client?  Client   Get the client currently under the mouse cursor.
---@field current_wibox?   Object   Get the wibox currently under the mouse cursor.
---@field current_widgets? Object[] Get the widgets currently under the mouse cursor.
---@field current_widget?  Object[] Get the topmost widget currently under the mouse cursor.
---@field current_widget_geometry?   Screen.geometry   Get the current widget geometry.
---@field current_widget_geometries? Screen.geometry[] Get the current widget geometries.
---@field is_left_mouse_button_pressed   boolean True if the left mouse button is pressed.
---@field is_right_mouse_button_pressed  boolean True if the right mouse button is pressed.
---@field is_middle_mouse_button_pressed boolean True if the middle mouse button is pressed.
mouse = {}

---Get the client or any object which is under the pointer.
---@return (Client|Object)? A client, wibox or nil.
function mouse.object_under_pointer() end

---@class Mouse.coords
---@field x? integer x coordinate
---@field y? integer y coordinate
---@field buttons boolean[] Button number as key and a boolean as value (if it is pressed)

---Set the mouse coords.
---@param coords_table { x: integer, y: integer } Table with x and y keys as mouse coordinates.
---@param silent? boolean Disable mouse::enter or mouse::leave events that could be
--triggered by the pointer when moving.
---@return Mouse.coords #The coords. It contains the x, y and buttons keys.
--Buttons contains the button number as key and a boolean as value (if it is pressed).
function mouse.coords(coords_table, silent) end

---Get the mouse coords.
---@return Mouse.coords #The coords. It contains the x, y and buttons keys.
--Buttons contains the button number as key and a boolean as value (if it is pressed).
function mouse.coords() end
