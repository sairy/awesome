---@meta

--TODO
---@class Client
---@class Drawable

---@class ScreenMod
---@field [string|integer] Screen
---@field primary Screen The primary screen
---@field automatic_factory boolean DON'T USE. If screen objects are created automatically when new viewports are detected.
---@operator call:Screen
screen = {}

---@param x      integer The X-coordinate for screen
---@param y      integer The Y-coordinate for screen
---@param width  integer The width for screen
---@param height integer The height for screen
---@return Screen # The new screen
function screen.fake_add(x, y, width, height) end

---Get the number of instances.
---@return integer[] # The number of screen objects alive
function screen.instances() end

---Get the number of screens
---@return integer # The screen count
function screen.count() end

---@alias ScreenAPISignal
--- | 'scanning' # AwesomeWM is about to scan for existing screens.
--- | 'scanned'  # AwesomeWM is done scanning for screens
--- | 'primary_changed' # Undocumented
--- | 'added'    # This signal is emitted when a new screen is added to the current setup.
--- | 'removed'  # This signal is emitted when a screen is removed from the setup
--- | 'list'     # This signal is emitted when the list of available screens changes
--- | 'swapped'  # When 2 screens are swapped
--- | 'property::viewports' # This signal is emitted when the list of physical screen viewport changes.
--- | 'request::desktop_decoration' # Emitted when a new screen is added
--- | 'request::wallpaper' # Emitted when a new screen needs a wallpaper
--- | 'request::create'    # When a new (physical) screen area has been added
--- | 'request::remove'    # When a physical monitor viewport has been removed
--- | 'request::resize'    # When a physical viewport resolution has changed or it has been replaced
--- | 'tag::history::update' # When the tag history changed
--- | string # User defined signal

---Disconnect from a signal
---@param name ScreenAPISignal  The name of the signal
---@param func function         The callback that should be disconnected
function screen.disconnect_signal(name, func) end

---Emit a signal
---@param name ScreenAPISignal  The name of the signal
---@param ... any               Extra arguments for the callback functions.
--Each connected function receives the object as first argument
--and then any extra arguments that are given to emit_signal().
function screen.emit_signal(name, ...) end

---Connect to a signal
---@param name ScreenAPISignal  The name of the signal
---@param func function         The callback to call when the signal is emitted
function screen.connect_signal(name, func) end

---Unit: pixel
---@class Screen.geometry
---@field x      integer
---@field y      integer
---@field width  integer
---@field height integer

---Unit: pixel
---Negative allowed: false
---@class Screen.margins
---@field top?    integer
---@field bottom? integer
---@field left?   integer
---@field right?  integer


---@class Screen.outputs
---@field mm_width?    integer
---@field mm_height?   integer
---@field mm_size?     number
---@field name?        string
---@field viewport_id? string
---@field dpi?         number
---@field inch_size?   number

---@class Screen
---@field geometry Screen.geometry  The screen coordinates
---@field index    integer          The internal screen number
---@field workarea Screen.geometry  The screen workarea
---@field tiling_area Screen.geometry  The area where clients can be tiled
---@field content table (raw_surface) Take a screenshot of the physical screen
---@field padding integer|Screen.margins The screen padding
---@field outputs table<string,Screen.outputs> A list of outputs for this screen with their size in mm
---@field clients Client[]  The list of visible clients for the screen
---@field hidden_clients  Client[] Get the list of clients assigned to the screen but not currently visible
---@field all_clients Client[]   All clients assigned to the screen
---@field tiled_clients Client[]  Tiled clients for the screen
---@field tags Tag[]  A list of all tags on the screen
---@field selected_tags Tag[] A list of all selected tags on the screen
---@field selected_tag Tag  The first selected tag
---@field dpi number  The number of pixels per inch of the screen
---@field minimum_dpi  number  The lowest density DPI from all of the (physical) outputs
---@field maximum_dpi  number  The highest density DPI from all of the (physical) outputs
---@field preferred_dpi number The preferred DPI from all of the (physical) outputs.
---@field mm_maximum_size number? The maximum diagonal size in millimeters
---@field mm_minimum_size  number? The minimum diagonal size in millimeters
---@field inch_maximum_size number? The maximum diagonal size in inches
---@field inch_minimum_size number? The minimum diagonal size in inches
local Screen = {}

---Remove a screen.
function Screen:fake_remove() end

---Resize a screen.
--Calling this will resize the screen even if it no longer matches the viewport size.
---@param x      integer The new X-coordinate for screen
---@param y      integer The new Y-coordinate for screen
---@param width  integer The new width for screen
---@param height integer The new height for screen
function Screen:fake_resize(x, y, width, height) end

---Swap a screen with another one in global screen list.
---@param s Screen A screen to swap with
function Screen:swap(s) end

---Get the square distance between a screen and a point
---@param x number X coordinate of point
---@param y number Y coordinate of point
---@return number # The squared distance of the screen to the provided point
function Screen:get_square_distance(x, y) end

---@class Screen.get_bounding_geometry.args
---@field honor_padding? boolean
---@field honor_workarea? boolean
---@field margins? integer|Screen.margins
---@field tag? Tag
---@field parent? Drawable
---@field bounding_rect? table

---Get a placement bounding geometry.
--This method computes the different variants of the "usable" screen geometry.
---@param args Screen.get_bounding_geometry.args The arguments
---@return Screen.geometry # A table with x, y, width and height.
function Screen:get_bounding_geometry(args) end

---Get the list of visible clients for the screen.
--This is used by screen.clients internally (with stacked=true).
---@param stacked boolean? Use stacking order? (top to bottom). Default true.
---@return Client[] # The clients list
function Screen:get_clients(stacked) end

---Get tiled clients for the screen.
--This is used by tiled_clients internally (with stacked=true).
---@param stacked boolean? Use stacking order? (top to bottom). Default true.
---@return Client[] # The clients list
function Screen:get_tiled_clients(stacked) end

---Split the screen into multiple screens.
--This is useful to turn ultrawide monitors into something
--more useful without fancy client layouts
---@param ratios? number[] The different ratios to split into.
--If none is provided, it is { 50, 50 }
---@param mode? 'vertical|horizontal' If none is specified, it will split along the longest axis.
function Screen:split(ratios, mode) end


---Emit a signal
---@param name ScreenAPISignal  The name of the signal
---@param ... any               Extra arguments for the callback functions.
--Each connected function receives the object as first argument
--and then any extra arguments that are given to emit_signal().
function Screen:emit_signal(name, ...) end

---Connect to a signal
---@param name ScreenAPISignal  The name of the signal
---@param func function         The callback to call when the signal is emitted
function Screen:connect_signal(name, func) end

---Connect to a signal weakly.
--This allows the callback function to be garbage collected and automatically
--disconnects the signal when that happens.
--Warning: Only use this function if you really, really, really know what you are doing.
---@param name ScreenAPISignal  The name of the signal
---@param func function         The callback that should be disconnected
function Screen:weak_connect_signal(name, func) end
