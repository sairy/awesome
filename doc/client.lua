---@meta

---@class ClientMod
---@field focus? Client The focused client or nil (in case there is none).
client = {}

---@alias Client.window_type
--- | 'desktop'       # The root client, it cannot be moved or resized.
--- | 'dock'          # A client attached to the side of the screen.
--- | 'splash'        # A client, usually without titlebar shown when an application starts.
--- | 'dialog'        # A dialog, see transient_for.
--- | 'menu'          # A context menu.
--- | 'toolbar'       # A floating toolbar.
--- | 'utility'       #
--- | 'dropdown_menu' # A context menu attached to a parent position.
--- | 'popup_menu'    # A context menu.
--- | 'notification'  # A notification popup.
--- | 'combo'         # A combobox list menu.
--- | 'dnd'           # A drag and drop indicator.
--- | 'normal'        # A normal application main window.

---@alias ClientAPISignal
--- | 'scanning' # Emitted when AwesomeWM is about to scan for existing clients.
--- | 'scanned' # Emitted when AwesomeWM is done scanning for clients
--- | 'focus' # Emitted when a client gains focus
--- | 'list' # Emitted before request::manage, after request::unmanage, and when clients swap.
--- | 'swapped' Emitted when 2 clients are swapped
--- | 'request::manage' # Emitted when a new client appears and gets managed by Awesome
--- | 'request::unmanage' # Emitted when a client is going away
--- | 'button::press' # Emitted when a mouse button is pressed in a client
--- | 'button::release' # Emitted when a mouse button is released in a client
--- | 'mouse::enter' # Emitted when the mouse enters a client.
--- | 'mouse::leave' # Emitted when the mouse leaves a client
--- | 'mouse::move' # Emitted when the mouse moves within a client
--- | 'request::activate' # Emitted when a client should get activated (focused and/or raised).
--- | 'request::autoactivate' # Emitted when an event could lead to the client being activated
--- | 'request::geometry' # Emitted when something request a client's geometry to be modified
--- | 'request::tag' # Emitted when a client requests to be moved to a tag or needs a new tag
--- | 'request::urgent' # Emitted when any client's urgent property changes
--- | 'request::default_mousebindings' # Emitted once to request default client mousebindings during the initial startup sequence
--- | 'request::default_keybindings' # Emitted once to request default client keybindings during the initial startup sequence
--- | 'tagged' # Emitted when a client gets tagged
--- | 'unfocus' # Emitted when a client gets unfocused
--- | 'untagged' # Emitted when a client gets untagged
--- | 'raised' # Emitted when the client is raised within its layer
--- | 'lowered' # Emitted when the client is lowered within its layer
--- | 'property::floating_geometry' # The last geometry when client was floating
--- | 'property::titlebars' # Emitted when a client need to get a titlebar
--- | 'request::border' # Emitted when the border client might need to be update
--- | string # User defined signal

---Get the number of instances
---@return integer # The number of client objects alive
function client.instances() end

---Get all clients into a table
---@param screen?  integer|Screen A screen number to filter clients on
---@param stacked? boolean Return clients in stacking order? (ordered from top to bottom)
---@return Client[] # A table with clients
function client.get(screen, stacked) end

---Disconnect from a signal
---@param name ClientAPISignal  The name of the signal
---@param func function         The callback that should be disconnected
function client.disconnect_signal(name, func) end

---Emit a signal
---@param name ClientAPISignal  The name of the signal
---@param ... any               Extra arguments for the callback functions.
--Each connected function receives the object as first argument
--and then any extra arguments that are given to emit_signal().
function client.emit_signal(name, ...) end

---Connect to a signal
---@param name ClientAPISignal  The name of the signal
---@param func function         The callback to call when the signal is emitted
function client.connect_signal(name, func) end

---@class Client
---@field window integer The X window id
---@field name   string The client title
---@field skip_taskbar boolean True if the client does not want to be in taskbar
---@field type Client.window_type The window type
---@field class string The client class (WM_Atom)
---@field instance string The client instance (WM_Atom)
---@field pid integer The client PID, if available
---@field role string The window role, if available
---@field machine string The machine the client is running on
---@field icon_name string The client name when iconified
---@field icon? string|Cairo.ImageSurface The client icon as a surface
---@field icon_sizes { [1|2]: integer }[] The available sizes of client icons. This is a table where each entry contains the width and height of an icon.
---@field screen Screen Client screen. The screen corresponds to the top-left corner of the window
---@field hidden boolean Define if the client must be hidden (Never mapped, invisible in taskbar)
---@field minimized boolean Define if the client must be iconified (Only visible in taskbar)
---@field size_hints_honor boolean Honor size hints, e.g. respect size ratio
---@field border_width? integer The client border width
---@field border_color? string|table|Cairo.Pattern The client border color
---@field urgent boolean Set to `true` when the client ask for attention.
---@field content table (raw_surface) A cairo surface for the client window content
---@field opacity number The client opacity
---@field ontop boolean The client is on top of every other windows
---@field above boolean The client is on above normal windows
---@field below boolean The client is on below normal windows
---@field fullscreen boolean The client is fullscreen or not
---@field maximized boolean The client is maximized (horizontally and vertically) or not
---@field maximized_horizontal boolean The client is maximized horizontally or not
---@field maximized_vertical boolean The client is maximized vertically or not
---@field transient_for? Client The client the window is transient for
---@field group_window integer Window identification unique to a group of windows
---@field leader_window integer Identification unique to windows spawned by the same command
---@field size_hints? table A table with size hints of the client.
---@field motif_wm_hints table The motif WM hints of the client.
---@field sticky boolean Set the client sticky (Available on all tags)
---@field modal boolean Indicate if the client is modal
---@field focusable boolean True if the client can receive the input focus
---@field shape_bounding? string|Cairo.ImageSurface The client's bounding shape as set by awesome as a (native) cairo surface
---@field shape_clip? string|Cairo.ImageSurface The client's bounding shape as set by awesome as a (native) cairo surface
---@field shape_input? string|Cairo.ImageSurface The client's input shape as set by awesome as a (native) cairo surface
---@field client_shape_bounding? string|Cairo.ImageSurface The client's bounding shape as set by the program as a (native) cairo surface
---@field client_shape_clip? string|Cairo.ImageSurface The client's clip shape as set by the program as a (native) cairo surface
---@field startup_id string The FreeDesktop StartId
---@field valid boolean If the client that this object refers to is still managed by awesome
---@field first_tag? Tag The first tag of the client
---@field buttons table Get or set mouse buttons bindings for a client
---@field keys table Get or set key bindings for a client
---@field marked boolean If a client is marked or not
---@field is_fixed boolean Return if a client has a fixed size or not
---@field immobilized_horizontal boolean Is the client immobilized horizontally?
---@field immobilized_vertical boolean Is the client immobilized vertically?
---@field floating boolean The client floating state
---@field x integer The x coordinates
---@field y integer The y coordinates
---@field width integer The width of the client
---@field height integer The height of the client
---@field dockable boolean If the client is dockable
---@field requests_no_titlebar boolean If the client requests not to be decorated with a titlebar
---@field shape table Set the client shape.
---@field active boolean Return true if the client is active (has focus)
local Client = {}


---Return client struts (reserved space at the edge of the screen).
--The struts area is a table with a `left`, `right`, `top` and `bottom` keys to define
--how much space of the screen workarea this client should reserve for itself.
--This corresponds to EWMH's `_NET_WM_STRUT` and `_NET_WM_STRUT_PARTIAL`.
---@param struts Screen.margins
---@return Screen.margins
---@see Client.geometry
---@see Screen.workarea
---@see Client.dockable
function Client:struts(struts) end

---Check if a client is visible on its screen.
---@return boolean # A boolean value, true if the client is visible, false otherwise
function Client:isvisible() end

---Kill a client.
--This method can be used to close (kill) a client using the X11 protocol.
--To use the POSIX way to kill a process, use awesome.kill (using the client pid property).
---@see awesome.kill
function Client:kill() end

---Swap a client with another one in global client list.
---@param c Client A client to swap with.
function Client:swap(c) end

---Access or set the client tags.
--Use the first_tag field to access the first tag of a client directly.
---@param tags_table? Tag[] A table with tags to set, or `nil` to get the current tags
---@return Tag[] A table with all tag
---@see Client.first_tag
---@see Client.toggle_tag
function Client:tags(tags_table) end

---Raise a client on top of others which are on the same layer
---@see Client.above
---@see Client.below
---@see Client.ontop
---@see Client.lower
function Client:raise() end

---Lower a client on bottom of others which are on the same layer
---@see Client.above
---@see Client.below
---@see Client.ontop
---@see Client.raise
function Client:lower() end

---Stop managing a client.
function Client:unmanage() end

---Return or set client geometry.
---@param geo? Screen.geometry
---@return Screen.geometry A table with client geometry and coordinates
---@see Client.struts
---@see Client.x
---@see Client.y
---@see Client.width
---@see Client.height
function Client:geometry(geo) end

--- Apply size hints to a size.
--This method applies the client size hints.
--The client will be resized according to the size hints as long as size_hints_honor is true.
--Regardless of the status of size_hints_honor, this method will return the size with
--the size hints applied.
---@param width integer Desired width of client
---@param height integer Desired height of client
---@return integer # Actual width of client
---@return integer # Actual height of client
---@see Client.size_hints
---@see Client.size_hints_honor
function Client:apply_size_hints(width, height) end

---Get the client's n-th icon.
--
--The icon index can be deternined by inspecting the icon_sizes property first.
--
--The user has the responsibility to test the value returned by this function to ensure
--an icon have been returned.
--
--It is recommended to use the awful.widget.clienticon widget when the client icon is
--used in a widget structure.
--
--Note that this function tests the provided index and raise an "invalid icon index" error
--if the provided index doesn't exist in the client's icons list (by raising an error,
--the function will be stopped and nothing will be returned to the caller).
---@param index integer The index in the list of icons to get
---@return Cairo.ImageSurface # A lightuserdata for a cairo surface.
--This reference must be destroyed!
---@see Client.icon_sizes
function Client:get_icon(index) end

---Jump to the given client.
--
--Takes care of focussing the screen, the right tag, etc.
---@param merge? boolean|fun(self: Client, first_tag?: Tag)
---@see Client.activate
---@see Client.active
function Client:jump_to(merge) end

---Append a keybinding
---@param key table # awful.key The key
---@see Client.remove_keybinding
---@see Client.append_mousebinding
---@see Client.remove_mousebinding
function Client:append_keybinding(key) end

---Remove a keybinding
---@param key table # awful.key The key
---@see Client.append_keybinding
---@see Client.append_mousebinding
---@see Client.remove_mousebinding
function Client:remove_keybinding(key) end

---Append a mousebinding
---@param button table # awful.button The button
---@see Client.append_keybinding
---@see Client.remove_keybinding
---@see Client.remove_mousebinding
function Client:append_mousebinding(button) end

---Remove a mousebinding
---@param button table # awful.button The button
---@see Client.append_keybinding
---@see Client.remove_keybinding
---@see Client.append_mousebinding
function Client:remove_mousebinding(button) end

---Move the client to the most significant layout position.
--
--This only affects tiled clients.
--It will shift all other client to fill the gap caused to by the move.
---@see Client.swap
---@see Client.to_secondary_section
function Client:to_primary_section() end

---Move the client to the least significant layout position.
--
--This only affects tiled clients.
--It will shift all other client to fill the gap caused to by the move.
---@see Client.swap
---@see Client.to_primary_section
function Client:to_secondary_section() end

---Move/resize a client relative to current coordinates
---@param x integer? The relative x coordinate
---@param y integer? The relative y coordinate
---@param w integer? The relative width
---@param h integer? The relative height
---@see Client.geometry
---@see Client.x
---@see Client.y
---@see Client.width
---@see Client.height
---@see Client.floating
function Client:relative_move(x, y, w, h) end

---Move a client to a tag.
---@param target Tag The tag to move the client to
---@see Client.tags
function Client:move_to_tag(target) end

---Toggle a tag on a client.
---@param target Tag The tag to move the client to
---@see Client.tags
function Client:toggle_tag(target) end

---Move a client to a screen. Default is next screen, cycling
---@param s? Screen The screen, default to current + 1
---@see ScreenMod
function Client:move_to_screen(s) end

---Find suitable tags for newly created clients.
--
--In most cases, the functionality you're actually looking for as a user will either be
--
--```lua
--c:tags(c.screen.selected_tags)
--```
--
--or
--
--```lua
--local s = awful.screen.focused()
-- c:move_to_screen(s)
-- c:tags(s.selected_tags)
--```
--
--Despite its naming, this is primarily used to tag newly created clients.
--As such, this method has no effect when applied to a client that already has tags assigned
--(except for emitting property::tag).
--
--Additionally, while it is a rare case, if the client's screen has no selected tags at the
--point of calling this method, it will fall back to the screen's full set of tags.
---@see Screen.selected_tags
function Client:to_selected_tags() end

---Get a matching transient_for client (if any).
---@param matcher fun():true? A function that should return true, if a matching parent client is found
---@return Client? # The matching parent client or nil
---@see Client.transient_for
---@see Client.modal
---@see Client.is_transient_for
function Client:get_transient_for_matching(matcher) end

---Is a client transient for another one?
--
--This will traverse the chain formed by the transient_for property of self until a client `c`
--with `c.transient_for == c2` is found. The found client `c` is returned.
--If no client is found, `nil` is returned.
---@param c2 Client The parent client to check.
---@return Client? # The parent client or nil
---@see Client.transient_for
---@see Client.modal
---@see Client.get_transient_for_matching
function Client:is_transient_for(c2) end

---@class Client.activate.args
---@field context? string|'other' Why was this activate called?
---@field raise? boolean Raise the client to the top of its layer and unminimize it (if needed).
---@field force? boolean Force the activation even for unfocusable clients.
---@field switch_to_tags? boolean
---@field switch_to_tag? boolean
---@field action? boolean Once activated, perform an action
---@field actions? Client.activate.args.actions
---@field toggle_minimization? boolean

---@alias Client.activate.args.actions
--- | 'mouse_move' # Move the client when the mouse cursor moves until the mouse buttons are release
--- | 'mouse_resize' # Resize the client when the mouse cursor moves until the mouse buttons are release
--- | 'mouse_center' # Move the mouse cursor to the center of the client if it isn't already within its geometry
--- | 'toggle_minimization' # If the client is already active, minimize it

---Activate (focus) a client.
--
--This method is the correct way to focus a client.
--While `client.focus = my_client` works and is commonly used in older code,
--it has some drawbacks.
--The most obvious one is that it bypasses the activate filters.
--It also doesn't handle minimized clients well and requires a lot of boilerplate code to
--make work properly.
---@param args Client.activate.args
---@see Client.active
function Client:activate(args) end

---Grant a permission for a client.
---@param permission string The permission name (just the name, no `request::`).
---@param context string The reason why this permission is requested
function Client:grant(permission, context) end

---Deny a permission for a client.
---@param permission string The permission name (just the name, no `request::`).
---@param context string The reason why this permission is requested
function Client:deny(permission, context) end

---Emit a signal
---@param name ClientAPISignal  The name of the signal
---@param ... any               Extra arguments for the callback functions.
--Each connected function receives the object as first argument
--and then any extra arguments that are given to emit_signal().
function Client:emit_signal(name, ...) end

---Connect to a signal
---@param name ClientAPISignal  The name of the signal
---@param func function         The callback to call when the signal is emitted
function Client:connect_signal(name, func) end

---Connect to a signal weakly.
--This allows the callback function to be garbage collected and automatically
--disconnects the signal when that happens.
--Warning: Only use this function if you really, really, really know what you are doing.
---@param name ClientAPISignal  The name of the signal
---@param func function         The callback that should be disconnected
function Client:weak_connect_signal(name, func) end
