---@meta

---@class TagMod
tag = {}

---@alias TagAPISignal
--- | 'request::select' Emitted when a tag requests to be selected
--- | 'request::default_layouts' This signal is emitted to request the list of default layouts
--- | 'request::layouts' This signals is emitted when a tag needs layouts for the first time
--- | 'tagged' Emitted when a client gets tagged with this tag
--- | 'untagged' Emitted when a client gets untagged with this tag
--- | 'cleared' Emitted when all clients are removed from the tag
--- | 'property::urgent' Emitted when the number of urgent clients on this tag changes
--- | 'property::urgent_count' Emitted when the number of urgent clients on this tag changes
--- | 'request::screen' Emitted when a screen is removed
--- | 'removal-pending' Emitted after request::screen if no new screen has been set.
--The tag will be deleted, this is a last chance to move its clients before they are
--sent to a fallback tag. Connect to request::screen if you wish to salvage the tag.
--- | string # User defined signal

---Get the number of instances.
---@return integer # The number of tag objects alive
function tag.instances() end

---Disconnect from a signal
---@param name TagAPISignal The name of the signal
---@param func function     The callback that should be disconnected
function tag.disconnect_signal(name, func) end

---Emit a signal
---@param name TagAPISignal The name of the signal
---@param ... any           Extra arguments for the callback functions.
--Each connected function receives the object as first argument
--and then any extra arguments that are given to emit_signal().
function tag.emit_signal(name, ...) end

---Connect to a signal
---@param name TagAPISignal The name of the signal
---@param func function     The callback to call when the signal is emitted
function tag.connect_signal(name, func) end

---@class awful.LayoutParams
---@field workarea Screen.geometry All clients must be placed within this area
---@field geometry Screen.geometry A table with the screen geometry
---@field clients  Client[] A list of the clients to place
---@field screen   Screen The screen
---@field padding  Screen.margins
---@field useless_gap integer The space that will be removed from the clients.
---@field geometries { [Client]: Screen.geometry }  Empty. Place the client as key and preferred geometry as value. Do not call :geometry() directly.

---@class Tag
---@field name string Tag name
---@field selected boolean True if the tag is selected to be viewed
---@field activated boolean True if the tag is active and can be used
---@field index integer The tag index
---@field screen Screen The tag screen
---@field master_width_factor number The tag master width factor
---@field layout awful.layout|fun(params: awful.LayoutParams) The tag client layout
---@field layouts? awful.layout[] The (proposed) list of available layouts for this tag
---@field volatile boolean Define if the tag must be deleted when the last client is untagged
---@field gap integer The gap (spacing, also called useless_gap) between clients
---@field gap_single_client boolean Enable gaps for a single client
---@field master_fill_policy 'expand'|'master_width_factor' Set size fill policy for the master client(s).
---@field master_count integer Set the number of master windows
---@field icon? Cairo.ImageSurface Set the tag icon
---@field column_count integer Set the number of columns
local Tag = {}

---Get or set the clients attached to this tag
---@param clients_table? Client[] None or a table of clients to set as being tagged
--with this tag
---@return Client[] # A table with the clients attached to this tag
function Tag:clients(clients_table) end

---Swap 2 tags
---@param tag2 Tag
function Tag:swap(tag2) end

---Remove all tagged clients
---@param args { fallback_tag?: Tag, allow_untagged?: boolean }
function Tag:clear(args) end

---Delete a tag
---@param fallback_tag? Tag     Tag to assign stickied tags to
---@param force?        boolean Move even non-sticky clients to the fallback tag
---@return boolean # Returns true if the tag is successfully deleted.
--If there are no clients exclusively on this tag then delete it.
--Any stickied clients are assigned to the optional 'fallback_tag'.
--If after deleting the tag there is no selected tag,
--try and restore from history or select the first tag on the screen
function Tag:delete(fallback_tag, force) end

---View only a tag
function Tag:view_only() end

---Emit a signal
---@param name TagAPISignal The name of the signal
---@param ... any           Extra arguments for the callback functions.
--Each connected function receives the object as first argument
--and then any extra arguments that are given to emit_signal().
function Tag:emit_signal(name, ...) end

---Connect to a signal
---@param name TagAPISignal The name of the signal
---@param func function     The callback to call when the signal is emitted
function Tag:connect_signal(name, func) end

---Connect to a signal weakly.
--This allows the callback function to be garbage collected and automatically
--disconnects the signal when that happens.
--Warning: Only use this function if you really, really, really know what you are doing.
---@param name TagAPISignal The name of the signal
---@param func function     The callback that should be disconnected
function Tag:weak_connect_signal(name, func) end
