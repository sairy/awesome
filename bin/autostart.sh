#!/bin/sh

restart() {
    for p in "$@"; do
        pkill "$p"
        setsid -f "$p"
    done
}

pamixer --get-volume >/dev/null 2>&1 &
xautolock -time 25 -locker slock &

restart redshift &

fcitx5 -r &
# keepassxc &
flameshot &
/lib/polkit-kde-authentication-agent-1 &
picom -b &
